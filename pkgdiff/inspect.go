package pkgdiff

import (
	"github.com/cavaliercoder/go-rpm"
)

// PkgInspect
type PkgInspect struct {
	bpkg *rpm.PackageFile
	upkg *rpm.PackageFile
}

// NewPkgInspect creates an instance of package inspector
func NewPkgInspect(basePkg *rpm.PackageFile, updatePkg *rpm.PackageFile) *PkgInspect {
	pkg := new(PkgInspect)
	pkg.bpkg, pkg.upkg = basePkg, updatePkg

	return pkg
}

// Diff two packages
func (ipt *PkgInspect) Diff() *UpdateDiff {
	inspectable := NewUpdateDiff(ipt.upkg)
	bpkgIndex := make(map[string]rpm.FileInfo)
	upkgIndex := make(map[string]rpm.FileInfo)

	// Index base
	for _, finfo := range ipt.bpkg.Files() {
		bpkgIndex[finfo.Name()] = finfo
	}

	// Index update
	for _, finfo := range ipt.upkg.Files() {
		upkgIndex[finfo.Name()] = finfo
	}

	// Get newly introduced files and changed
	for _, fileInfo := range upkgIndex {
		baseFileInfo, ex := bpkgIndex[fileInfo.Name()]
		if !ex {
			inspectable.AddNewFile(fileInfo)
		} else {
			if baseFileInfo.Digest() != fileInfo.Digest() {
				// Here source diffing might be possible, like AddChangedFile(fileInfo, diffSrc) etc.
				inspectable.AddChangedFile(fileInfo)
			}
		}
	}

	// Get removed files with the update
	for _, fileInfo := range bpkgIndex {
		updateFileInfo := upkgIndex[fileInfo.Name()]
		if updateFileInfo.Name() == "" {
			inspectable.AddDeletedFile(fileInfo)
		}
	}

	return inspectable
}
