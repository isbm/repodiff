package pkgflow

import (
	"strings"

	"github.com/gdamore/tcell/v2"
	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	"github.com/isbm/crtview"
)

// SearchWindow class
type SearchWindow struct {
	dashboard  *DashBoardController
	filterText *crtview.InputField
	bgColor    tcell.Color
	fgColor    tcell.Color
	container  *crtview.Flex
	wm         *crtview.WindowManager
	window     *crtview.Window

	*crtview.Box
	wzlib_logger.WzLogger
}

// NewSearchWindow constructor
func NewSearchWindow(dashboard *DashBoardController) *SearchWindow {
	aw := new(SearchWindow)
	aw.dashboard = dashboard

	return aw.initUI()
}

// Initialise user interface
func (aw *SearchWindow) initUI() *SearchWindow {
	aw.bgColor = tcell.ColorMediumBlue
	aw.fgColor = tcell.ColorLightSkyBlue

	aw.Box = crtview.NewBox()

	aw.filterText = crtview.NewInputField()
	aw.filterText.SetBackgroundColor(aw.bgColor)
	aw.filterText.SetFieldTextColor(tcell.ColorBlack)
	aw.filterText.SetFieldBackgroundColor(tcell.ColorWhite)
	aw.filterText.SetLabelColor(aw.fgColor)
	aw.filterText.SetLabel(" Search:")
	aw.filterText.SetChangedFunc(func(text string) {
		aw.seek()
	})
	aw.filterText.SetInputCapture(
		func(event *tcell.EventKey) *tcell.EventKey {
			switch event.Key() {
			case tcell.KeyEsc:
				aw.dashboard.PkgList.Select(1, 0)
				aw.Hide()
			case tcell.KeyEnter:
				aw.Hide()
			}

			return event
		})

	aw.container = crtview.NewFlex().SetDirection(crtview.FlexRow)
	aw.container.AddItem(aw.filterText, 0, 1, false)
	aw.window = crtview.NewWindow(aw.filterText).SetBackgroundColor(aw.bgColor)
	aw.window.SetTitleColor(aw.fgColor)
	aw.window.SetBorderColor(aw.fgColor)

	aw.wm = crtview.NewWindowManager()
	aw.wm.Add(aw.window)
	aw.window.SetSize(40, 10)
	aw.window.SetPosition(10, 5)

	return aw
}

func (aw *SearchWindow) seek() {
	if aw.filterText.GetText() == "" {
		aw.dashboard.PkgList.Select(1, 0)
	} else {
		for name := range aw.dashboard.PkgList.GetPackageIndex() {
			if strings.HasPrefix(name, aw.filterText.GetText()) {
				aw.dashboard.PkgList.Select(aw.dashboard.PkgList.GetPackageOffsetByName(name), 0)
				return
			}
		}
	}
}

func (aw *SearchWindow) Show() *SearchWindow {
	aw.filterText.SetText("")
	aw.dashboard.Pages.SwitchToPage(aw.dashboard.SEARCH_POPUP)
	aw.dashboard.Pages.ShowPage(aw.dashboard.ROOT_WINDOW)
	aw.dashboard.app.SetFocus(aw.filterText)
	return aw
}

func (aw *SearchWindow) Hide() *SearchWindow {
	aw.dashboard.Pages.SwitchToPage(aw.dashboard.ROOT_WINDOW)
	aw.dashboard.app.SetFocus(aw.dashboard.PkgList)
	return aw
}

func (aw *SearchWindow) Draw(screen tcell.Screen) {
	if screen.Colors() == 0x100 {
		aw.window.SetBorderColor(tcell.ColorLightSkyBlue)
		aw.window.SetBackgroundColor(tcell.ColorMediumBlue)
	}

	//w, h := screen.Size()
	//aw.window.SetSize(aw.maxwidth, aw.maxheight)
	//aw.window.SetPosition((w/2)-(aw.maxwidth/2), (h/2)-(aw.maxheight/2))
	aw.window.SetPosition(10, 5)
	aw.window.SetSize(30, 3)

	aw.wm.Draw(screen)
}
