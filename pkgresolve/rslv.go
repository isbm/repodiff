package pkgresolve

import (
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strings"

	"github.com/go-yaml/yaml"
	"gitlab.com/isbm/repodiff/gitbind"
	"gitlab.com/isbm/repodiff/reposcan"
)

type PkgAcceptanceResolver struct {
	pkgIndex   []string
	pkgs       map[string]*ResolvedPackage
	filename   string
	gitresolve *gitbind.PkgDiffGitResolver
}

func NewPkgAcceptanceResolver() *PkgAcceptanceResolver {
	return new(PkgAcceptanceResolver).flushData()
}

func (par *PkgAcceptanceResolver) flushData() *PkgAcceptanceResolver {
	par.pkgIndex = make([]string, 0)
	par.pkgs = make(map[string]*ResolvedPackage)
	return par
}

// GetPackage returns an existing record or transparently creates new.
func (par *PkgAcceptanceResolver) GetPackage(name string) *ResolvedPackage {
	pstat := par.pkgs[name]
	if pstat == nil {
		pstat = NewResolvedPackage(name)
		par.pkgs[name] = pstat
		par.pkgIndex = append(par.pkgIndex, pstat.Name())
		sort.Strings(par.pkgIndex)
	}
	return pstat
}

// CheckPackage returns an existing record or transparently creates new.
func (par *PkgAcceptanceResolver) CheckPackage(name string) *ResolvedPackage {
	pstat := par.pkgs[name]
	if pstat == nil {
		pstat = NewResolvedPackage(name)
	}
	return pstat
}

// Parse the review document overlay
func (par *PkgAcceptanceResolver) Parse(text string) error {
	par.flushData()
	src := make(map[string]map[string]map[string]string)
	if err := yaml.Unmarshal([]byte(text), &src); err != nil {
		return err
	}
	for pkgName, pkgFiles := range src {
		par.pkgIndex = append(par.pkgIndex, pkgName)

		rsPkg := NewResolvedPackage(pkgName)
		par.pkgs[rsPkg.Name()] = rsPkg

		for fname, fstats := range pkgFiles {
			switch fstats["status"] {
			case "accepted":
				rsPkg.GetFile(fname).Accept().SetComment(fstats["comment"])
			case "rejected":
				rsPkg.GetFile(fname).Reject().SetComment(fstats["comment"])
			case "review":
				rsPkg.GetFile(fname).Review().SetComment(fstats["comment"])
			default:
				rsPkg.GetFile(fname).Reset()
			}
		}
	}
	sort.Strings(par.pkgIndex)
	return nil
}

// Render the review document overlay.
// This also will drop all FileStatus references,
// if and only if there all of the files has STATUS_UNKNOWN
func (par *PkgAcceptanceResolver) Render() []byte {
	var out strings.Builder
	for _, pkgName := range par.pkgIndex {
		pkgRef := par.pkgs[pkgName]
		if !par.isValid(pkgRef) {
			continue
		}

		out.WriteString(fmt.Sprintf("%s:\n", pkgName))
		for _, fname := range pkgRef.fileIndex {
			out.WriteString(fmt.Sprintf("  %s:\n", fname))
			fstat := pkgRef.files[fname]
			switch fstat.GetStatus() {
			case STATUS_ACCEPTED:
				out.WriteString("    status: accepted\n")
			case STATUS_REJECTED:
				out.WriteString("    status: rejected\n")
			case STATUS_IN_REVIEW:
				out.WriteString("    status: review\n")
			}
			out.WriteString(fmt.Sprintf("    comment: %s\n", fstat.GetComment()))
		}
	}
	return []byte(out.String())
}

// Check if the resolved package is valid
func (par *PkgAcceptanceResolver) isValid(pkgref *ResolvedPackage) bool {
	if len(pkgref.fileIndex) > 0 {
		for _, pkgref := range par.pkgs {
			if len(pkgref.fileIndex) > 0 {
				for _, filestat := range pkgref.files {
					if filestat.GetStatus() != STATUS_UNKNOWN {
						// Hadouken!
						// https://twitter.com/jasonyeojs/status/758316026853371904
						return true
					}
				}
			}
		}
	}
	return false
}

// Setup resolver
func (par *PkgAcceptanceResolver) Setup(filename string, gitresolve *gitbind.PkgDiffGitResolver) (*PkgAcceptanceResolver, error) {
	par.gitresolve = gitresolve
	par.filename = filename
	_, err := os.Stat(par.filename)
	if err != nil {
		return par, ioutil.WriteFile(par.filename, []byte(""), 0644)
	}

	data, err := ioutil.ReadFile(par.filename)
	if err != nil {
		return par, err
	}
	return par, par.Parse(string(data))
}

// Save the current content
func (par *PkgAcceptanceResolver) Save(message string) error {
	if err := ioutil.WriteFile(par.filename, par.Render(), 0644); err != nil {
		return fmt.Errorf("Error writing file: %s", err.Error())
	}
	// Todo: Git commits not always faulty. If a commit is simply not needed, it should be skipped.
	par.gitresolve.CommitFile(par.filename, message)
	return nil
}

func (par *PkgAcceptanceResolver) FilterDiff(ld *reposcan.LayoutDiff) *reposcan.LayoutDiff {
	// TODO: walk through the entire diff and change it accordingly to the acceptance document
	return ld
}
