package pkgflow

import (
	"github.com/gdamore/tcell/v2"
	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	"github.com/isbm/crtview"
	"github.com/isbm/go-nanoconf"
	"gitlab.com/isbm/repodiff/pkgresolve"
	"gitlab.com/isbm/repodiff/reposcan"
)

type TerminalSize struct {
	Width  int
	Height int
}

type UIDashboard struct {
	dbc *DashBoardController
	app *crtview.Application

	wzlib_logger.WzLogger
}

func NewUIDashboard(resolver *pkgresolve.PkgAcceptanceResolver, conf *nanoconf.Config) *UIDashboard {
	dshb := new(UIDashboard)
	dshb.app = crtview.NewApplication()
	dshb.dbc = NewDashBoardController(dshb.app, resolver, conf)
	dshb.app.SetBeforeDrawFunc(func(screen tcell.Screen) bool {
		dshb.dbc.term.Width, dshb.dbc.term.Height = screen.Size()
		return false
	})

	return dshb
}

func (dashboard *UIDashboard) onAppKeypress(event *tcell.EventKey) *tcell.EventKey {
	return event
}

func (dashboard *UIDashboard) LoadRepodiff(diff *reposcan.LayoutDiff) *UIDashboard {
	dashboard.dbc.diff = dashboard.dbc.resolver.FilterDiff(diff)

	// Init UI default widget settings
	dashboard.dbc.loadPackageList()
	dashboard.dbc.Filter.SetCurrentOption(0)

	return dashboard
}

func (dashboard *UIDashboard) setDefaultSelectedWidget() {
	dashboard.app.SetFocus(dashboard.dbc.PkgList)
}

func (dashboard *UIDashboard) Run() {
	dashboard.app.SetInputCapture(dashboard.onAppKeypress)
	dashboard.app.SetRoot(dashboard.dbc.Pages, true)
	dashboard.setDefaultSelectedWidget()
	if err := dashboard.app.Run(); err != nil {
		dashboard.GetLogger().Errorf("Error running app: %s", err.Error())
	}
}
