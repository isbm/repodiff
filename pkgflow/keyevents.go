package pkgflow

import (
	"strings"

	"github.com/gdamore/tcell/v2"
	"github.com/isbm/crtview"
)

func (dbc *DashBoardController) onPkgListKeypress(event *tcell.EventKey) *tcell.EventKey {
	row, _ := dbc.PkgList.GetSelection()
	rows := dbc.PkgList.GetRowCount()
	if event.Key() == tcell.KeyUp && row == 1 || event.Key() == tcell.KeyBacktab {
		dbc.PkgList.SetSelectable(false, false)
		dbc.app.SetFocus(dbc.Filter)
	} else if event.Key() == tcell.KeyLeft {
		dbc.PkgList.Select(1, 0)
	} else if event.Key() == tcell.KeyRight {
		return tcell.NewEventKey(tcell.KeyEnd, ' ', 0)
	} else if event.Key() == tcell.KeyDown {
		if row+1 < rows {
			dbc.PkgList.Select(row+1, 0)
		}
	} else if event.Key() == tcell.KeyUp {
		dbc.PkgList.Select(row-1, 0)
	} else if event.Key() == tcell.KeyCtrlD {
		meta := dbc.PkgList.GetSelectedPackageMeta()
		if meta != nil {
			dbc.PkgDepViewer.LoadDependencies(meta.Package.Requires())
			dbc.PkgDepViewer.deplist.Select(0, 0)
			dbc.Pages.SwitchToPage(dbc.DEP_VIEWER)
			dbc.Pages.ShowPage(dbc.ROOT_WINDOW)
		} // else pop-up warning?
	} else if event.Key() == tcell.KeyCtrlS {
		dbc.searchWindow.Show()
	} else if event.Key() == tcell.KeyCtrlH {
		dbc.helpPopup.Show()
	}
	return event
}

func (dbc *DashBoardController) onPkgListSelectionChanged(row int, cell int) {
	meta := dbc.PkgList.GetSelectedPackageMeta()
	if meta != nil {
		dbc.PkgInfo.LoadPackageInfo(dbc.PkgList.GetSelectedPackageMeta())
	}
}

func (dbc *DashBoardController) onPkgListSelected(row int, cell int) {
	dbc.PkgCntViewer.SetPackage(dbc.PkgList.GetSelectedPackageMeta())
	dbc.Pages.SwitchToPage(dbc.CONTENT_VIEWER)
	dbc.Pages.ShowPage(dbc.ROOT_WINDOW)
}

func (dbc *DashBoardController) onFilterKeypress(e *tcell.EventKey) *tcell.EventKey {
	if e.Key() == tcell.KeyRight {
		dbc.PkgList.SetSelectable(true, false)
		dbc.PkgList.Select(0, 0)
		dbc.app.SetFocus(dbc.PkgList)
	}
	return e
}

func (dbc *DashBoardController) onFilterSelected(index int, option *crtview.DropDownOption) {
	dbc.SetPackageListMode(strings.ToLower(option.GetText()))
	dbc.loadPackageList()
	dbc.PkgList.SetSelectable(true, false)
	dbc.PkgList.Select(1, 0)
	dbc.app.SetFocus(dbc.PkgList)
}
