package pkgflow

import (
	"fmt"
	"os"
	"strings"

	"github.com/gdamore/tcell/v2"
	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	"github.com/isbm/crtview"
	"github.com/isbm/textwrap"
	"gitlab.com/isbm/repodiff/pkgresolve"
	"gitlab.com/isbm/repodiff/reposcan"
)

const (
	MODE_FILE = iota
	MODE_PACKAGE
)

type PkgDiffResolve struct {
	*crtview.Box
	wzlib_logger.WzLogger

	dashboard *DashBoardController
	text      *crtview.TextView
	window    *crtview.Window

	// Comment combo (no multi-line text input)
	cmtLine1 *crtview.InputField
	cmtLine2 *crtview.InputField
	cmtLine3 *crtview.InputField

	maxwidth           int
	maxwidthspace      int
	maxheight          int
	lastSelected       int
	confirmationMode   int
	confirmationStatus int

	currentFile *PkgFileInfo

	acceptButton   *crtview.Button
	rejectButton   *crtview.Button
	inReviewButton *crtview.Button
}

func NewPkgDiffResolve(dashboard *DashBoardController) *PkgDiffResolve {
	pdr := new(PkgDiffResolve)
	pdr.Box = crtview.NewBox()
	pdr.dashboard = dashboard
	pdr.confirmationMode = MODE_FILE

	pdr.maxwidthspace = 10
	pdr.maxheight = 12

	pdr.initUI()
	return pdr
}

func (pdr *PkgDiffResolve) initUI() {
	pdr.acceptButton = crtview.NewButton("Accept")
	pdr.acceptButton.SetInputCapture(func(e *tcell.EventKey) *tcell.EventKey {
		switch e.Key() {
		case tcell.KeyUp:
			pdr.dashboard.app.SetFocus(pdr.cmtLine3)
		case tcell.KeyTAB, tcell.KeyRight:
			pdr.dashboard.app.SetFocus(pdr.rejectButton)
		case tcell.KeyBacktab, tcell.KeyLeft:
			pdr.dashboard.app.SetFocus(pdr.inReviewButton)
		case tcell.KeyEnter:
			pdr.SetAccepted()
			pdr.close(true)
		case tcell.KeyESC:
			pdr.close(false)
		case tcell.KeyCtrlN:
			pdr.setupNewComment()
		}
		return e
	})

	pdr.rejectButton = crtview.NewButton("Reject")
	pdr.rejectButton.SetInputCapture(func(e *tcell.EventKey) *tcell.EventKey {
		switch e.Key() {
		case tcell.KeyUp:
			pdr.dashboard.app.SetFocus(pdr.cmtLine3)
		case tcell.KeyTAB, tcell.KeyRight:
			pdr.dashboard.app.SetFocus(pdr.inReviewButton)
		case tcell.KeyBacktab, tcell.KeyLeft:
			pdr.dashboard.app.SetFocus(pdr.acceptButton)
		case tcell.KeyEnter:
			pdr.SetRejected()
			pdr.close(true)
		case tcell.KeyESC:
			pdr.close(false)
		case tcell.KeyCtrlN:
			pdr.setupNewComment()
		}
		return e
	})
	pdr.inReviewButton = crtview.NewButton("In Review")
	pdr.inReviewButton.SetInputCapture(func(e *tcell.EventKey) *tcell.EventKey {
		switch e.Key() {
		case tcell.KeyUp:
			pdr.dashboard.app.SetFocus(pdr.cmtLine3)
		case tcell.KeyTAB, tcell.KeyRight:
			pdr.dashboard.app.SetFocus(pdr.acceptButton)
		case tcell.KeyBacktab, tcell.KeyLeft:
			pdr.dashboard.app.SetFocus(pdr.rejectButton)
		case tcell.KeyEnter:
			pdr.SetInReview()
			pdr.close(true)
		case tcell.KeyESC:
			pdr.close(false)
		case tcell.KeyCtrlN:
			pdr.setupNewComment()
		}
		return e
	})

	spacer := crtview.NewTextView()
	spacer.SetText(" ")
	buttonBar := crtview.NewFlex()
	buttonBar.SetDirection(crtview.FlexColumn)
	buttonBar.AddItem(spacer, 1, 1, false)
	buttonBar.AddItem(pdr.acceptButton, 0, 1, true)
	buttonBar.AddItem(spacer, 1, 1, false)
	buttonBar.AddItem(pdr.rejectButton, 0, 1, false)
	buttonBar.AddItem(spacer, 1, 1, false)
	buttonBar.AddItem(pdr.inReviewButton, 0, 1, false)
	buttonBar.AddItem(spacer, 1, 1, false)

	pdr.text = crtview.NewTextView().SetDynamicColors(true)

	// Comment lines
	pdr.cmtLine1 = crtview.NewInputField()
	pdr.cmtLine1.SetText("")
	pdr.cmtLine1.SetLabel(" Comment (1):")
	pdr.cmtLine1.SetInputCapture(func(e *tcell.EventKey) *tcell.EventKey {
		switch e.Key() {
		case tcell.KeyDown, tcell.KeyEnter:
			pdr.dashboard.app.SetFocus(pdr.cmtLine2)
		case tcell.KeyESC:
			pdr.close(false)
		}
		return e
	})

	pdr.cmtLine2 = crtview.NewInputField()
	pdr.cmtLine2.SetLabel("         (2) ")
	pdr.cmtLine2.SetText("")
	pdr.cmtLine2.SetInputCapture(func(e *tcell.EventKey) *tcell.EventKey {
		switch e.Key() {
		case tcell.KeyUp:
			pdr.dashboard.app.SetFocus(pdr.cmtLine1)
		case tcell.KeyDown, tcell.KeyEnter:
			pdr.dashboard.app.SetFocus(pdr.cmtLine3)
		case tcell.KeyESC:
			pdr.close(false)
		}
		return e
	})

	pdr.cmtLine3 = crtview.NewInputField()
	pdr.cmtLine3.SetLabel("         (3) ")
	pdr.cmtLine3.SetText("")
	pdr.cmtLine3.SetInputCapture(func(e *tcell.EventKey) *tcell.EventKey {
		switch e.Key() {
		case tcell.KeyUp:
			pdr.dashboard.app.SetFocus(pdr.cmtLine2)
		case tcell.KeyDown, tcell.KeyEnter:
			pdr.dashboard.app.SetFocus(pdr.acceptButton)
		case tcell.KeyESC:
			pdr.close(false)
		}
		return e
	})

	commentLine1 := crtview.NewFlex()
	commentLine1.SetDirection(crtview.FlexColumn)
	commentLine1.AddItem(pdr.cmtLine1, 0, 1, false)
	commentLine1.AddItem(spacer, 1, 1, false)

	commentLine2 := crtview.NewFlex()
	commentLine2.SetDirection(crtview.FlexColumn)
	commentLine2.AddItem(pdr.cmtLine2, 0, 1, false)
	commentLine2.AddItem(spacer, 1, 1, false)

	commentLine3 := crtview.NewFlex()
	commentLine3.SetDirection(crtview.FlexColumn)
	commentLine3.AddItem(pdr.cmtLine3, 0, 1, false)
	commentLine3.AddItem(spacer, 1, 1, false)

	// Window
	layout := crtview.NewFlex()
	layout.SetDirection(crtview.FlexRow)
	layout.AddItem(pdr.text, 0, 1, false)
	layout.AddItem(commentLine1, 1, 1, false)
	layout.AddItem(commentLine2, 1, 1, false)
	layout.AddItem(commentLine3, 1, 1, false)
	layout.AddItem(spacer, 1, 1, false)
	layout.AddItem(buttonBar, 1, 0, true)

	pdr.window = crtview.NewWindow(layout).SetPositionCenter().Hide()
	pdr.text.SetTextAlign(crtview.AlignCenter)
}

func (pdr *PkgDiffResolve) SetLastSelected(row int) {
	pdr.lastSelected = row
}

func (pdr *PkgDiffResolve) close(refresh bool) {
	if refresh {
		pdr.dashboard.PkgCntViewer.Refresh()
	}
	pdr.currentFile = nil
	pdr.GetWindow().Hide()
	pdr.dashboard.app.SetFocus(pdr.dashboard.PkgCntViewer.fileList)
	pdr.dashboard.PkgCntViewer.fileList.SetSelectable(true, false)
	pdr.dashboard.PkgCntViewer.fileList.Select(pdr.lastSelected, 0)
	pdr.cmtLine1.SetText("")
	pdr.cmtLine2.SetText("")
	pdr.cmtLine3.SetText("")
	pdr.lastSelected = 0
}

// SetAccepted sets current file of the package as accepted
func (pdr *PkgDiffResolve) SetAccepted() {
	if pdr.currentFile == nil {
		return
	}

	comment := pdr.GetCustomComment()
	if pdr.confirmationMode == MODE_FILE {
		pdr.registerCurrentFile().Accept().SetComment(pdr.getFormattedComment(pkgresolve.STATUS_ACCEPTED))
		if comment == "" {
			comment = fmt.Sprintf("Accept file %s", pdr.currentFile.PkgFileInfo.Name())
		}
	} else {
		pdr.registerCurrentPackage(pkgresolve.STATUS_ACCEPTED)
		if comment == "" {
			comment = fmt.Sprintf("Accept package %s", pdr.currentFile.Package.Name())
		}
	}

	pdr.checkError(pdr.dashboard.resolver.Save(comment))
}

// SetRejected sets current file of the package as rejected
func (pdr *PkgDiffResolve) SetRejected() {
	if pdr.currentFile == nil {
		return
	}

	comment := pdr.GetCustomComment()
	if pdr.confirmationMode == MODE_FILE {
		pdr.registerCurrentFile().Reject().SetComment(pdr.getFormattedComment(pkgresolve.STATUS_REJECTED))
		if comment == "" {
			comment = fmt.Sprintf("Reject file %s", pdr.currentFile.PkgFileInfo.Name())
		}
	} else {
		pdr.registerCurrentPackage(pkgresolve.STATUS_REJECTED)
		if comment == "" {
			comment = fmt.Sprintf("Reject package %s", pdr.currentFile.Package.Name())
		}
	}

	pdr.checkError(pdr.dashboard.resolver.Save(comment))
}

// SetInReview sets current file of the package as in-review
func (pdr *PkgDiffResolve) SetInReview() {
	if pdr.currentFile == nil {
		return
	}

	comment := pdr.GetCustomComment()
	if pdr.confirmationMode == MODE_FILE {
		pdr.registerCurrentFile().Review().SetComment(pdr.getFormattedComment(pkgresolve.STATUS_IN_REVIEW))
		if comment == "" {
			comment = fmt.Sprintf("Set for the review file %s", pdr.currentFile.PkgFileInfo.Name())
		}
	} else {
		pdr.registerCurrentPackage(pkgresolve.STATUS_IN_REVIEW)
		if comment == "" {
			comment = fmt.Sprintf("Set for the review package %s", pdr.currentFile.Package.Name())
		}
	}

	pdr.checkError(pdr.dashboard.resolver.Save(comment))
}

func (pdr *PkgDiffResolve) checkError(err error) {
	if err != nil {
		pdr.dashboard.app.Stop()
		pdr.GetLogger().Errorf("Error writing release acceptance file: %s", err.Error())
		os.Exit(1)
	}
}

// An alias
func (pdr *PkgDiffResolve) registerCurrentFile() *pkgresolve.FileStatus {
	return pdr.dashboard.resolver.GetPackage(pdr.currentFile.Package.Name()).GetFile(pdr.currentFile.PkgFileInfo.Name())
}

// Register the entire package per a status
func (pdr *PkgDiffResolve) registerCurrentPackage(status int) {
	var pkgstatLabel string
	if pdr.confirmationStatus == reposcan.STATUS_REMOVED {
		pkgstatLabel = "removed"
	} else if pdr.confirmationStatus == reposcan.STATUS_NEW {
		pkgstatLabel = "new"
	} else {
		pdr.GetLogger().Fatal("DEBUG: Unknown reposcan status")
	}
	rpkg := pdr.dashboard.resolver.GetPackage(pdr.currentFile.Package.Name())
	for _, nfo := range pdr.currentFile.Package.Files() {
		switch status {
		case pkgresolve.STATUS_ACCEPTED:
			rpkg.GetFile(nfo.Name()).Accept().SetComment(fmt.Sprintf("With the %s %s package", pkgstatLabel, pdr.currentFile.Package.Name()))
		case pkgresolve.STATUS_REJECTED:
			rpkg.GetFile(nfo.Name()).Reject().SetComment(fmt.Sprintf("With the %s %s package", pkgstatLabel, pdr.currentFile.Package.Name()))
		case pkgresolve.STATUS_IN_REVIEW:
			rpkg.GetFile(nfo.Name()).Review().SetComment(fmt.Sprintf("With the %s %s package", pkgstatLabel, pdr.currentFile.Package.Name()))
		}
	}
}

// GetCustomComment returns a content of the comment lines, if any.
func (pdr *PkgDiffResolve) GetCustomComment() string {
	var cmt strings.Builder
	for _, item := range strings.Split(fmt.Sprintf("%s %s %s", pdr.cmtLine1.GetText(), pdr.cmtLine2.GetText(), pdr.cmtLine3.GetText()), " ") {
		item = strings.TrimSpace(item)
		if item != "" {
			cmt.WriteString(item + " ")
		}
	}

	return strings.TrimSpace(cmt.String())
}

// An alias
func (pdr *PkgDiffResolve) getFormattedComment(state int) string {
	comment := pdr.GetCustomComment()
	if comment == "" { // no comment added
		stateMsg := ""
		switch state {
		case pkgresolve.STATUS_ACCEPTED:
			stateMsg = "Accepting"
		case pkgresolve.STATUS_REJECTED:
			stateMsg = "Rejecting"
		case pkgresolve.STATUS_IN_REVIEW:
			stateMsg = "Reviewing"
		}
		comment = fmt.Sprintf("%s the change for file %s in version %s",
			stateMsg, pdr.currentFile.PkgFileInfo.Name(), pdr.currentFile.Package.Version())
	}

	return comment
}

func (pdr *PkgDiffResolve) setWindowSize(constraint string) {
	mw := len(constraint) + pdr.maxwidthspace*2
	pdr.maxwidth = 50
	if mw > pdr.maxwidth {
		pdr.maxwidth = mw
	}
	pdr.window.SetSize(pdr.maxwidth, pdr.maxheight)
}

func (pdr *PkgDiffResolve) setupNewComment() {
	for _, wdg := range []*crtview.InputField{pdr.cmtLine1, pdr.cmtLine2, pdr.cmtLine3} {
		wdg.SetText("")
	}
	pdr.dashboard.app.SetFocus(pdr.cmtLine1)
}

func (pdr *PkgDiffResolve) loadCommentBlock() {
	cmt := strings.TrimSpace(pdr.dashboard.resolver.CheckPackage(pdr.currentFile.Package.Name()).
		CheckFile(pdr.currentFile.PkgFileInfo.Name()).GetComment())
	if cmt != "" {
		wds := []*crtview.InputField{pdr.cmtLine1, pdr.cmtLine2, pdr.cmtLine3}
		for line, data := range textwrap.NewTextWrap().SetWidth(0x20).Wrap(cmt) {
			if line > 2 {
				line = 2
				data = wds[line].GetText() + " " + data
			}
			wds[line].SetText(data)
		}
	}
}

func (pdr *PkgDiffResolve) ResolvePackage(pkgmeta *reposcan.PackageMeta) {
	pdr.confirmationMode = MODE_PACKAGE
	pdr.confirmationStatus = pkgmeta.Status
	pdr.currentFile = &PkgFileInfo{Package: pkgmeta.Package, NeedResolution: false}
	pdr.text.SetText(fmt.Sprintf("Set the resolution status to the package:\n\"%s\"\n\n[darkgrey](ESC to cancel)", pkgmeta.Package.Name()))
	pdr.dashboard.app.SetFocus(pdr.acceptButton)
	pdr.setWindowSize(pkgmeta.Package.Name())
	pdr.window.Show()
}

func (pdr *PkgDiffResolve) ResolveFile(pkgFile *PkgFileInfo) {
	pdr.confirmationMode = MODE_FILE
	pdr.currentFile = pkgFile
	pathPkgFile := fmt.Sprintf("%s/%s", pdr.currentFile.Package.Name(), pdr.currentFile.PkgFileInfo.Name())
	pdr.dashboard.app.SetFocus(pdr.acceptButton)
	pdr.text.SetText(fmt.Sprintf("Set the resolution status to:\n\"%s\"\n\n[darkgrey](Ctrl+N for a new comment, ESC to cancel)", pathPkgFile))
	pdr.loadCommentBlock()
	pdr.setWindowSize(pathPkgFile)
	pdr.window.Show()
}

func (pdr *PkgDiffResolve) GetWindow() *crtview.Window {
	return pdr.window
}
