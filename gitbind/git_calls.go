package gitbind

import (
	"bytes"
	"fmt"
	"net/mail"
	"os"
	"path"
	"strings"
	"time"

	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	wzlib_subprocess "github.com/infra-whizz/wzlib/subprocess"
)

type GSCLogItem struct {
	Commit  string
	Merge   []string
	Author  *mail.Address
	Message []string
	Date    time.Time
}

func (gli *GSCLogItem) GetShortMessage() string {
	if len(gli.Message) > 0 {
		return gli.Message[0]
	}
	return ""
}

func (gli *GSCLogItem) GetFormattedDate() string {
	return fmt.Sprintf("%v", gli.Date)
}

type GSCProjectStatus struct {
	Modified  []string
	New       []string
	Deleted   []string
	Untracked []string
}

func NewGSCProjectStatus() *GSCProjectStatus {
	stat := new(GSCProjectStatus)
	stat.Modified = make([]string, 0)
	stat.New = make([]string, 0)
	stat.Deleted = make([]string, 0)
	stat.Untracked = make([]string, 0)
	return stat
}

type GitCaller struct {
	repoPath string
	wzlib_logger.WzLogger
}

func NewGitCaller() *GitCaller {
	return new(GitCaller)
}

// SetRepoPath
func (gitcall *GitCaller) SetRepoPath(repoPath string) *GitCaller {
	gitcall.repoPath = repoPath
	return gitcall
}

// Call git with specific params. All calls are blocking.
func (gitcall *GitCaller) Call(args ...string) error {
	if err := wzlib_subprocess.ExecCommand("git", args...).Run(); err != nil {
		gitcall.GetLogger().Errorf("Error calling Git: %s", err.Error())
		return err
	}
	return nil
}

func (gitcall *GitCaller) GetUserEmail() string {
	cmd := wzlib_subprocess.ExecCommand("git", "config", "user.email")
	var out bytes.Buffer
	cmd.Stdout = &out
	if err := cmd.Run(); err != nil {
		gitcall.GetLogger().Panic(err.Error())
	}
	return strings.TrimSpace(out.String())
}

// GetDefaultBranch from Git. Note, "default branch" is GitHub's terminology.
func (gitcall *GitCaller) GetDefaultBranch() string {
	cmd := wzlib_subprocess.ExecCommand("git", "symbolic-ref", "refs/remotes/origin/HEAD")
	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Run()
	branchPath := strings.Split(strings.TrimSpace(out.String()), "/")
	return branchPath[len(branchPath)-1]
}

func (gitcall *GitCaller) GetCurrentBranch() string {
	cmd := wzlib_subprocess.ExecCommand("git", "symbolic-ref", "--short", "HEAD")
	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Run()
	return strings.TrimSpace(out.String())
}

func (gitcall *GitCaller) getWorkingDir() string {
	wdir, err := os.Getwd()
	if err != nil {
		gitcall.GetLogger().Panic(err.Error())
	}
	return wdir
}

// Gets data about file or a commit, navigatable by that file.
// Args can have flags with "::" prefix.
// Current flags:
//    ::nofile -- Will not include the very filename to the git command
func (gitcall *GitCaller) getFileData(filename string, trim bool, args ...string) ([]string, error) {
	var flagNoFile bool

	wd := gitcall.getWorkingDir()
	p := path.Dir(filename)
	if !strings.HasPrefix(p, "/") {
		p = path.Join(gitcall.repoPath, p)
	}

	if err := os.Chdir(path.Clean(p)); err != nil {
		os.Chdir(wd)
		return nil, fmt.Errorf("Unable to find \"%s\" package", path.Base(p))
	}

	// Check if target exist
	_, err := os.Stat(path.Base(filename))
	if os.IsNotExist(err) {
		os.Chdir(wd)
		return nil, fmt.Errorf("File \"%s\" cannot be found in the Git repository", path.Base(filename))
	}

	exeArgs := []string{}
	for _, arg := range args {
		if strings.HasPrefix(arg, "::") {
			switch arg {
			case "::nofile":
				flagNoFile = true
			}
			continue
		}
		exeArgs = append(exeArgs, arg)
	}

	if !flagNoFile {
		exeArgs = append(exeArgs, path.Base(filename))
	}

	cmd := wzlib_subprocess.ExecCommand("git", exeArgs...)
	var out bytes.Buffer
	cmd.Stdout = &out
	if err := cmd.Run(); err != nil {
		os.Chdir(wd)
		return nil, err
	}

	lines := []string{}
	for _, line := range strings.Split(out.String(), "\n") {
		if trim {
			line = strings.TrimSpace(line)
		}

		if line == "" {
			continue
		}

		lines = append(lines, line)
	}

	if err := os.Chdir(wd); err != nil {
		return nil, fmt.Errorf("Unable to return to the working directory")
	}

	return lines, nil
}

func (gitcall *GitCaller) parseGitLog(data []string) []*GSCLogItem {
	out := make([]*GSCLogItem, 0)
	var item *GSCLogItem
	for _, line := range data {
		probe := strings.Split(line, " ")

		if probe[0] == "commit" && len(probe) == 2 && len(probe[1]) == 0x28 {
			if item != nil {
				out = append(out, item)
			}
			item = &GSCLogItem{Commit: probe[1], Message: []string{}}
		} else if probe[0] == "Merge:" {
			item.Merge = append([]string{}, strings.Split(probe[1], " ")...)
		} else if probe[0] == "Author:" {
			item.Author, _ = mail.ParseAddress(strings.SplitN(line, ": ", 2)[1])
			if item.Author == nil {
				item.Author = &mail.Address{Name: "*error*", Address: ""}
			}
		} else if probe[0] == "Date:" {
			item.Date, _ = time.Parse("Mon Jan 2 15:04:05 2006 -0700", strings.TrimSpace(line[5:]))
		} else {
			item.Message = append(item.Message, strings.TrimSpace(line))
		}
	}
	if item != nil {
		out = append(out, item)
	}

	return out
}

// GetCommitData returns the entire content of a specific commit
func (gitcall *GitCaller) GetCommitData(filename string, commit string) ([]string, error) {
	return gitcall.getFileData(filename, false, "show", commit, "::nofile")
}

// GetLog history from the git repo on the specific file
func (gitcall *GitCaller) GetLog(filename string) ([]*GSCLogItem, error) {
	data, err := gitcall.getFileData(filename, false, "log") //, "--full-history")
	if err != nil {
		return nil, err
	}
	return gitcall.parseGitLog(data), err
}

// GetBlame data from the git repo
func (gitcall *GitCaller) GetBlame(filename string) ([]string, error) {
	return gitcall.getFileData(filename, true, "blame")
}

// GetProjectStatus is similar to what OSC does, but this returns status for the Git repo
func (gitcall *GitCaller) GetProjectStatus() (*GSCProjectStatus, error) {
	cmd := wzlib_subprocess.ExecCommand("git", "status", "--porcelain=v1")
	var out bytes.Buffer
	cmd.Stdout = &out
	if err := cmd.Run(); err != nil {
		return nil, err
	}

	stat := NewGSCProjectStatus()
	for _, line := range strings.Split(strings.TrimSpace(out.String()), "\n") {
		fstat := strings.SplitN(line, " ", 2)
		if len(fstat) == 2 {
			fileStatus := strings.ToLower(fstat[0])
			fileName := strings.TrimSpace(fstat[1])
			fmt.Println("Status:", fileStatus, "Name:", fileName)
			switch fileStatus {
			case "m":
				stat.Modified = append(stat.Modified, fileName)
			case "d":
				stat.Deleted = append(stat.Deleted, fileName)
			case "??":
				if !strings.HasPrefix(fileName, ".osc/") {
					stat.New = append(stat.New, fileName)
				}
			default:
				gitcall.GetLogger().Errorf("Unknown Git status '%s' for file '%s'. Please report a bug", fileStatus, fileName)
			}

		}
	}

	return stat, nil
}
