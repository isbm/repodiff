package pkgflow

import (
	"fmt"
	"strings"

	"github.com/gdamore/tcell/v2"
	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	"github.com/isbm/crtview"
	"gitlab.com/isbm/repodiff/gitbind"
)

// GitBlameView class
type GitBlameView struct {
	git        *gitbind.GitCaller
	blameList  *crtview.Table
	dashboard  *DashBoardController
	window     *crtview.Window
	blameLines int
	alertError error

	*crtview.Box
	wzlib_logger.WzLogger
}

// NewGitBlameView constructor
func NewGitBlameView(dashboard *DashBoardController) *GitBlameView {
	gbv := new(GitBlameView)
	gbv.dashboard = dashboard
	gbv.git = gitbind.NewGitCaller().SetRepoPath(dashboard.GetConfig().Root().String("git.repo.path", ""))

	return gbv.initUI()
}

// Initialise user interface
func (gbv *GitBlameView) initUI() *GitBlameView {
	gbv.Box = crtview.NewBox()
	gbv.blameList = crtview.NewTable()

	// Window
	layout := crtview.NewFlex().SetDirection(crtview.FlexRow)
	layout.AddItem(gbv.blameList, 0, 1, false)

	gbv.window = crtview.NewWindow(layout).SetPositionCenter().SetFullscreen(true).SetMarginBorder(4, 7, 4, 7).Hide()

	gbv.blameList.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {
		case tcell.KeyEsc:
			gbv.GetWindow().Hide()
			gbv.dashboard.app.SetFocus(gbv.dashboard.PkgCntViewer.fileList)
		}
		return event
	})

	return gbv
}

// Blame some file within the Git repo. The current chdir
// should be within that Git repo.
func (gbv *GitBlameView) Blame(pkg *PkgFileInfo) *GitBlameView {
	gbv.blameList.Clear()
	gbv.alertError = nil
	gbv.window.SetTitle(fmt.Sprintf(" Path: %s/%s ", pkg.Package.Name(), pkg.PkgFileInfo.Name()))

	var blame []string
	blame, gbv.alertError = gbv.git.GetBlame(fmt.Sprintf("%s/%s", pkg.Package.Name(), pkg.PkgFileInfo.Name()))
	if gbv.alertError != nil {
		return gbv
	}

	commitColors := map[int]string{0: "yellow", 1: "lime"}
	infoColors := map[int]string{0: "olive", 1: "green"}
	hlColors := map[int]string{0: "#ffffff", 1: "#aaaaaa"}
	checksum, commitColor, infoColor, hlColor := "", 0, 0, 0
	for idx, line := range blame {
		lset := strings.SplitN(line, ")", 2)
		cset := strings.SplitN(lset[0], " ", 2)
		if checksum != cset[0] {
			commitColor++
			infoColor++
			hlColor++
			checksum = cset[0]
		}

		cline := fmt.Sprintf("[%s]%s [%s]%s) [gray]\u2502[%s]%s", commitColors[commitColor%2],
			checksum, infoColors[infoColor%2], cset[1], hlColors[hlColor%2], lset[1])
		gbv.blameList.SetCell(idx, 0, crtview.NewTableCell(cline).SetExpansion(1))
	}
	gbv.blameLines = len(blame)

	return gbv
}

func (gbv *GitBlameView) Show() *GitBlameView {
	if gbv.alertError != nil {
		gbv.dashboard.PkgCntViewer.alertWindow.SetText("Error: " + gbv.alertError.Error()).Show()
	} else {
		gbv.dashboard.app.SetFocus(gbv.blameList)
		gbv.GetWindow().Show()
	}
	return gbv
}

func (gbv *GitBlameView) GetWindow() *crtview.Window {
	return gbv.window
}
