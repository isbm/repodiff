package reposcan

import (
	"fmt"
	"strings"

	"github.com/cavaliercoder/go-rpm"
	"github.com/karrick/godirwalk"
	"gitlab.com/isbm/repodiff/pkgdiff"
)

func (rld *RepoLayoutDiff) scanRPMRepo() error {
	// Get structure of the base repo
	rld.GetLogger().Infof("Base repository at %s", rld.repodiff.BaseRepoPath)
	if err := godirwalk.Walk(rld.repodiff.BaseRepoPath, &godirwalk.Options{
		Callback: func(pkgPath string, entry *godirwalk.Dirent) error {
			if !strings.HasSuffix(pkgPath, ".src.rpm") {
				return nil
			}

			rld.GetLogger().Infof("Reading %s", entry.Name())
			pkg, err := rpm.OpenPackageFile(pkgPath)
			if err != nil {
				rld.GetLogger().Errorf("Error opening RPM package: %s", err.Error())
				return err
			}
			rld.baseRPMs[pkg.Name()] = pkg
			return nil
		}, Unsorted: true}); err != nil {
		return err
	}

	// Get structure of the update repository
	rld.GetLogger().Infof("Update repository at %s", rld.repodiff.UpdateRepoPath)
	if err := godirwalk.Walk(rld.repodiff.UpdateRepoPath, &godirwalk.Options{
		Callback: func(pkgPath string, entry *godirwalk.Dirent) error {
			if !strings.HasSuffix(pkgPath, ".src.rpm") {
				return nil
			}

			rld.GetLogger().Infof("Reading %s", entry.Name())
			pkg, err := rpm.OpenPackageFile(pkgPath)
			if err != nil {
				rld.GetLogger().Errorf("Error opening RPM package: %s", err.Error())
				return err
			}
			rld.updateRPMs[pkg.Name()] = pkg
			bpkg := rld.baseRPMs[pkg.Name()]
			if bpkg == nil {
				rld.repodiff.NewPackages = append(rld.repodiff.NewPackages,
					&PackageMeta{Package: pkg, Status: STATUS_NEW})
			}
			return nil
		}, Unsorted: true}); err != nil {
		return err
	}

	rld.findRemovedPackages()
	rld.findChangedPackages()

	// Find unchanged packages
	for _, pkg := range rld.repodiff.NewPackages {
		_, ex := rld.updateRPMs[pkg.Package.Name()]
		if ex {
			delete(rld.updateRPMs, pkg.Package.Name())
		}
	}
	for _, pkg := range rld.updateRPMs {
		rld.repodiff.UnchangedPackages = append(rld.repodiff.UnchangedPackages,
			&PackageMeta{Package: pkg, Status: STATUS_UNCHANGED})
	}

	return nil
}

// Get changed packages (from the shared common)
func (rld *RepoLayoutDiff) findChangedPackages() {
	for _, pkg := range rld.baseRPMs {
		changed := true

		// Check if the package is in the new packages
		for _, npkg := range rld.repodiff.NewPackages {
			if npkg.Package.Name() == pkg.Name() {
				changed = false
				delete(rld.updateRPMs, pkg.Name())
				break
			}
		}

		// Check if the package is in the removed packages
		for _, rpkg := range rld.repodiff.RemovedPackages {
			if rpkg.Package.Name() == pkg.Name() {
				changed = false
				delete(rld.updateRPMs, pkg.Name())
				break
			}
		}

		if changed {
			packageDiff := pkgdiff.NewPkgInspect(pkg, rld.updateRPMs[pkg.Name()]).Diff()
			if packageDiff.HasAnyChanges() {
				delete(rld.updateRPMs, pkg.Name())
				rld.repodiff.ChangedPackages = append(rld.repodiff.ChangedPackages, packageDiff)
			}
		}
	}
}

// Get removed packages
func (rld *RepoLayoutDiff) findRemovedPackages() {
	for _, pkg := range rld.baseRPMs {
		bpkg := rld.updateRPMs[pkg.Name()]
		if bpkg == nil {
			rld.repodiff.RemovedPackages = append(rld.repodiff.RemovedPackages,
				&PackageMeta{Package: pkg, Status: STATUS_REMOVED})
		}
	}
}

func (rld *RepoLayoutDiff) scanDebRepo() error {
	return fmt.Errorf("Support for the Debian repositories is not yet implement.")
}

// Scan base repository
func (rld *RepoLayoutDiff) scanBaseRepo() error {
	switch strings.ToLower(rld.repoType) {
	case "rpm":
		rld.GetLogger().Infof("Scanning RPM-based repo")
		return rld.scanRPMRepo()
	case "deb":
		return rld.scanDebRepo()
	default:
		return fmt.Errorf("Unknown repository type: %s", rld.repoType)
	}
}
