package pkgdiff

import "github.com/cavaliercoder/go-rpm"

// UpdateDiff type
type UpdateDiff struct {
	Package      *rpm.PackageFile
	ChangedFiles []rpm.FileInfo
	NewFiles     []rpm.FileInfo
	DeletedFiles []rpm.FileInfo
}

// NewUpdateDiff constructor
func NewUpdateDiff(pkg *rpm.PackageFile) *UpdateDiff {
	ipt := new(UpdateDiff)
	ipt.Package = pkg
	ipt.ChangedFiles = make([]rpm.FileInfo, 0)
	ipt.NewFiles = make([]rpm.FileInfo, 0)
	ipt.DeletedFiles = make([]rpm.FileInfo, 0)

	return ipt
}

// AddNewFile
func (dfr *UpdateDiff) AddNewFile(finfo rpm.FileInfo) {
	dfr.NewFiles = append(dfr.NewFiles, finfo)
}

// AddChangedFile
func (dfr *UpdateDiff) AddChangedFile(finfo rpm.FileInfo) {
	dfr.ChangedFiles = append(dfr.ChangedFiles, finfo)
}

// AddDeletedFile
func (dfr *UpdateDiff) AddDeletedFile(finfo rpm.FileInfo) {
	dfr.DeletedFiles = append(dfr.DeletedFiles, finfo)
}

// HasChanges
func (dfr *UpdateDiff) HasAnyChanges() bool {
	return len(dfr.DeletedFiles)+len(dfr.NewFiles)+len(dfr.ChangedFiles) > 0
}
