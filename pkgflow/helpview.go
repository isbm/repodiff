package pkgflow

import (
	"github.com/gdamore/tcell/v2"
	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	"github.com/isbm/crtview"
)

const helpText = `[black]List of all shortcuts in "repodiff".

[white::b]Main window

[yellow::b]  Ctrl+H    [black::d]This help
[yellow::b]  Ctrl+D    [black::d]List of package dependencies
[yellow::b]  Ctrl+S    [black::d]Search for a package
[yellow::b]  ENTER     [black::d]View content of a package
[yellow::b]  Ctrl+C    [black::d]Quit


[white::b]Content view

[yellow::b]  Ctrl+B    [black::d]Review changes of the item (Git Blame)
[yellow::b]  Ctrl+L    [black::d]List of commits, ENTER to see the diff
[yellow::b]  ENTER     [black::d]Accept, reject or hold the change
[yellow::b]  ESC       [black::d]Close


[white::b]Dependencies view

[yellow::b]  ESC       [black::d]Close
`

// HelpWindow class
type HelpWindow struct {
	dashboard *DashBoardController
	bgColor   tcell.Color
	fgColor   tcell.Color
	content   *crtview.TextView
	container *crtview.Flex
	wm        *crtview.WindowManager
	window    *crtview.Window

	*crtview.Box
	wzlib_logger.WzLogger
}

// NewHelpWindow constructor
func NewHelpWindow(dashboard *DashBoardController) *HelpWindow {
	hw := new(HelpWindow)
	hw.dashboard = dashboard

	return hw.initUI()
}

// Initialise user interface
func (hw *HelpWindow) initUI() *HelpWindow {
	hw.bgColor = tcell.ColorDarkGray
	hw.fgColor = tcell.ColorBlack

	hw.Box = crtview.NewBox()

	hw.content = crtview.NewTextView().
		SetBackgroundColor(hw.bgColor).
		SetTextColor(hw.fgColor).
		SetDynamicColors(true)
	hw.content.SetText(helpText)
	hw.content.SetInputCapture(
		func(event *tcell.EventKey) *tcell.EventKey {
			switch event.Key() {
			case tcell.KeyEsc:
				hw.Hide()
			}

			return event
		})

	hw.container = crtview.NewFlex().SetDirection(crtview.FlexRow)
	hw.container.AddItem(hw.content, 0, 1, false)
	hw.window = crtview.NewWindow(hw.content).
		SetBackgroundColor(hw.bgColor).
		SetTitle(" Help ").
		SetTitleColor(hw.fgColor).
		SetBorderColor(hw.bgColor)

	hw.wm = crtview.NewWindowManager()
	hw.wm.Add(hw.window)
	hw.window.SetSize(60, 25)

	return hw
}

func (hw *HelpWindow) Show() *HelpWindow {
	hw.dashboard.Pages.SwitchToPage(hw.dashboard.HELP_POPUP)
	hw.dashboard.Pages.ShowPage(hw.dashboard.ROOT_WINDOW)
	hw.dashboard.app.SetFocus(hw.content)
	return hw
}

func (hw *HelpWindow) Hide() *HelpWindow {
	hw.dashboard.Pages.SwitchToPage(hw.dashboard.ROOT_WINDOW)
	hw.dashboard.app.SetFocus(hw.dashboard.PkgList)
	return hw
}

func (hw *HelpWindow) Draw(screen tcell.Screen) {
	if screen.Colors() == 0x100 {
		hw.window.SetBorderColor(hw.fgColor)
		hw.window.SetBackgroundColor(hw.bgColor)
	}

	w, h := screen.Size()
	iw, ih := hw.window.GetSize()
	hw.window.SetPosition((w/2)-(iw/2), (h/2)-(ih/2))

	hw.wm.Draw(screen)
}
