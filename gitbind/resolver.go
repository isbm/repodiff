package gitbind

import (
	"fmt"
	"os"
	"strings"
	"time"

	wzlib_logger "github.com/infra-whizz/wzlib/logger"
)

type PkgDiffGitResolver struct {
	git *GitCaller
	wzlib_logger.WzLogger
}

func NewPkgDiffGitResolver() *PkgDiffGitResolver {
	pdgr := new(PkgDiffGitResolver)
	pdgr.git = NewGitCaller()

	return pdgr
}

// InitBranch creates a branch instead of working on master
func (pdgr *PkgDiffGitResolver) InitBranch() *PkgDiffGitResolver {
	user := pdgr.git.GetUserEmail()
	if !strings.Contains(user, "@") {
		pdgr.GetLogger().Error("Unable to find current configured user. Add one?")
		os.Exit(1)
	}
	user = strings.Split(user, "@")[0]

	defaultBranch := pdgr.git.GetDefaultBranch()
	branch := pdgr.git.GetCurrentBranch()
	if defaultBranch != branch && strings.HasPrefix(branch, user+"_") {
		return pdgr
	}

	st := time.Now()
	branch = fmt.Sprintf("%s_review_%d%02d%02d%02d%02d%02d", user,
		st.Year(), st.Month(), st.Day(), st.Hour(), st.Minute(), st.Second())

	pdgr.GetLogger().Debugf("Creating branch: %s", branch)
	if err := pdgr.git.Call("checkout", "-b", branch); err != nil {
		pdgr.GetLogger().Errorf("Error creating new branch: %s", err.Error())
		os.Exit(1)
	}

	return pdgr
}

// CommitFile to the current repository
func (pdgr *PkgDiffGitResolver) CommitFile(name string, message string) error {
	if err := pdgr.git.Call("commit", name, "-m", message); err != nil {
		return fmt.Errorf("Error committing \"%s\" with the message \"%s\": %s", name, message, err.Error())
	}
	return nil
}
