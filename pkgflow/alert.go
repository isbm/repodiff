package pkgflow

import (
	"github.com/gdamore/tcell/v2"
	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	"github.com/isbm/crtview"
)

// Alert Window class
type AlertWindow struct {
	dashboard   *DashBoardController
	window      *crtview.Window
	message     *crtview.TextView
	closeButton *crtview.Button
	parentFocus crtview.Primitive
	bgColor     tcell.Color
	fgColor     tcell.Color

	defaultWidth  int
	defaultHeight int

	*crtview.Box
	wzlib_logger.WzLogger
}

// NewAlertWindow constructor
func NewAlertWindow(dashboard *DashBoardController, parent crtview.Primitive) *AlertWindow {
	aw := new(AlertWindow)
	aw.dashboard = dashboard
	aw.parentFocus = parent

	return aw.initUI()
}

// Initialise user interface
func (aw *AlertWindow) initUI() *AlertWindow {
	aw.defaultWidth = 40
	aw.defaultHeight = 10

	aw.bgColor = tcell.ColorRed
	aw.fgColor = tcell.ColorWhite

	aw.Box = crtview.NewBox()
	aw.message = crtview.NewTextView().SetBackgroundColor(aw.bgColor).SetTextColor(aw.fgColor).SetDynamicColors(true)

	aw.closeButton = crtview.NewButton("Close")
	aw.closeButton.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {
		case tcell.KeyEnter, tcell.KeyEsc:
			aw.Close()
		}
		return event
	})

	bbar := crtview.NewFlex().SetDirection(crtview.FlexColumn).SetBackgroundColor(aw.bgColor)
	bbar.AddItem(crtview.NewTextView().SetText(" ").SetBackgroundColor(aw.bgColor), 0, 1, false).
		AddItem(aw.closeButton, 10, 1, false).
		AddItem(crtview.NewTextView().SetText(" ").SetBackgroundColor(aw.bgColor), 0, 1, false)

	aw.window = crtview.NewWindow(
		crtview.NewFlex().
			SetDirection(crtview.FlexRow).AddItem(aw.message, 0, 1, false).AddItem(bbar, 1, 1, false)).
		SetPositionCenter().SetFullscreen(false).SetSize(aw.defaultWidth, aw.defaultHeight).Hide()

	aw.window.SetBackgroundColor(aw.bgColor).SetBorderColor(aw.fgColor).SetTitle(" Alert ")

	return aw
}

// SetText to the container
func (aw *AlertWindow) SetText(message string) *AlertWindow {
	aw.message.SetText(message)
	return aw
}

// Show the alert window
func (aw *AlertWindow) Show() *AlertWindow {
	aw.dashboard.app.SetFocus(aw.closeButton)
	aw.GetWindow().Show()
	return aw
}

// SetAlertSize sets the window size
func (aw *AlertWindow) SetAlertSize(w int, h int) *AlertWindow {
	aw.defaultWidth, aw.defaultHeight = w, h
	aw.window.SetSize(aw.defaultWidth, aw.defaultHeight)
	return aw
}

// Close the alert window
func (aw *AlertWindow) Close() *AlertWindow {
	aw.dashboard.app.SetFocus(aw.parentFocus)
	aw.GetWindow().Hide()
	return aw
}

func (aw *AlertWindow) GetWindow() *crtview.Window {
	return aw.window
}
