package pkgflow

import (
	"strings"

	"github.com/cavaliercoder/go-rpm"
	"github.com/gdamore/tcell/v2"
	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	"github.com/isbm/crtview"
)

type PkgDepView struct {
	*crtview.Box

	dashboard     *DashBoardController
	deplistHeader *crtview.Table
	deplist       *crtview.Table
	pack          *crtview.Flex
	wm            *crtview.WindowManager
	window        *crtview.Window
	closeButton   *crtview.Button

	bgColor tcell.Color
	fgColor tcell.Color

	maxwidth  int
	maxheight int
	wzlib_logger.WzLogger
}

func NewPkgDepView(dashboard *DashBoardController) *PkgDepView {
	pdv := new(PkgDepView)
	pdv.Box = crtview.NewBox()

	pdv.dashboard = dashboard
	pdv.bgColor = tcell.ColorBlue
	pdv.fgColor = tcell.ColorLightCyan

	pdv.maxheight = 10

	pdv.initUI()
	pdv.clear()

	return pdv
}

func (pdv *PkgDepView) initUI() {
	// Buttonbar
	pdv.closeButton = crtview.NewButton("Close")
	bbr := crtview.NewFlex()
	bbr.SetDirection(crtview.FlexColumn)

	bbr.AddItem(pdv.closeButton, 3, 1, false)
	sep := crtview.NewTextView()
	sep.SetText("")
	bbr.AddItem(sep, 0, 1, false)

	// Deplist Header
	pdv.deplistHeader = crtview.NewTable()
	pdv.deplistHeader.SetSelectable(false, false)
	pdv.deplistHeader.SetBackgroundColor(pdv.bgColor)

	// Deplist
	pdv.deplist = crtview.NewTable()
	pdv.deplist.SetSelectable(true, false)
	pdv.deplist.SetSeparator('\u2502')
	pdv.deplist.SetBackgroundColor(pdv.bgColor)
	pdv.deplist.SetBorderColor(pdv.fgColor)
	pdv.deplist.SetInputCapture(func(e *tcell.EventKey) *tcell.EventKey {
		if e.Key() == tcell.KeyESC {
			pdv.dashboard.Pages.SwitchToPage(pdv.dashboard.ROOT_WINDOW)
			pdv.dashboard.app.SetFocus(pdv.dashboard.PkgList)
		}
		return e
	})

	pdv.pack = crtview.NewFlex()
	pdv.pack.SetDirection(crtview.FlexRow)
	pdv.pack.AddItem(pdv.deplistHeader, 1, 1, false)
	pdv.pack.AddItem(pdv.deplist, 0, 1, true)

	// Window
	pdv.window = crtview.NewWindow(pdv.pack).SetBackgroundColor(pdv.bgColor)
	pdv.window.SetTitleColor(pdv.fgColor)
	pdv.window.SetBorderColor(pdv.fgColor)
	pdv.wm = crtview.NewWindowManager()
	pdv.wm.Add(pdv.window)

	pdv.window.SetSize(40, 10)
	pdv.window.SetPosition(10, 5)
}

func (pdv *PkgDepView) LoadDependencies(requires []rpm.Dependency) {
	pdv.clear()
	pdv.maxwidth = 0
	rqw, dpw := 0, 0
	for offset, dep := range requires {
		reqLabel := dep.Name()
		if strings.Contains(reqLabel, "(") {
			if dep.Version() != "" {
				reqLabel = strings.ReplaceAll(reqLabel, "(", ", "+dep.Version()+" [green](")
			}
		}

		reqLabel += " "
		depLabel := "-      "

		reqCell := crtview.NewTableCell(reqLabel)
		reqCell.SetExpansion(1)
		reqCell.SetTextColor(pdv.fgColor)

		depCell := crtview.NewTableCell(depLabel)
		depCell.SetTextColor(pdv.fgColor)

		pdv.deplist.SetCell(offset, 0, reqCell) // requires
		pdv.deplist.SetCell(offset, 1, depCell) // depends

		reqLabelLen := len(reqLabel)
		if strings.Contains(reqLabel, "[orange]") {
			reqLabelLen -= 8
		}
		if strings.Contains(reqLabel, "[lightgrey]") {
			reqLabelLen -= 11
		}

		if reqLabelLen > rqw {
			rqw = reqLabelLen
		}
		if len(depLabel) > dpw {
			dpw = len(depLabel)
		}
	}
	pdv.maxwidth = rqw + dpw + 5

	// Filler
	if len(requires) < pdv.maxheight {
		for i := 0; i < pdv.maxheight-len(requires)-3; i++ {
			filler := crtview.NewTableCell("")
			filler.SetSelectable(false)
			pdv.deplist.SetCell(len(requires)+i, 0, filler) // requires
			pdv.deplist.SetCell(len(requires)+i, 1, filler) // depends
		}
	}

	// Header
	offset := 0
	hdrStyle := new(tcell.Style).Foreground(tcell.ColorYellow).Bold(true)
	for label, wd := range map[string]int{"Requires": rqw, "Depends": dpw} {
		if len(label) < wd {
			label += strings.Repeat(" ", wd-len(label))
		}
		hdr := crtview.NewTableCell(label)
		hdr.SetSelectable(false)
		hdr.SetStyle(hdrStyle)
		pdv.deplistHeader.SetCell(0, offset, hdr)
		offset++
	}
}

func (pdv *PkgDepView) clear() {
	pdv.deplistHeader.Clear()
	pdv.deplist.Clear()
}

func (pdv *PkgDepView) Focus(func(p crtview.Primitive)) {
	pdv.dashboard.app.SetFocus(pdv.deplist)
}

func (pdv *PkgDepView) Draw(screen tcell.Screen) {
	if screen.Colors() == 0x100 {
		pdv.window.SetBorderColor(tcell.ColorLightSkyBlue)
		pdv.window.SetBackgroundColor(tcell.ColorMediumBlue)
		pdv.deplist.SetBackgroundColor(tcell.ColorMediumBlue)
		pdv.deplistHeader.SetBackgroundColor(tcell.ColorMediumBlue)
	}

	w, h := screen.Size()
	pdv.window.SetSize(pdv.maxwidth, pdv.maxheight)
	pdv.window.SetPosition((w/2)-(pdv.maxwidth/2), (h/2)-(pdv.maxheight/2))
	pdv.wm.Draw(screen)
}
