package pkgflow

import (
	"fmt"
	"strings"

	"github.com/gdamore/tcell/v2"
	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	"github.com/isbm/crtview"
	"gitlab.com/isbm/repodiff/gitbind"
)

// GitCommitView class
type GitCommitView struct {
	git           *gitbind.GitCaller
	commitContent *crtview.TextView
	dashboard     *DashBoardController
	window        *crtview.Window
	alertError    error

	*crtview.Box
	wzlib_logger.WzLogger
}

// NewGitCommitView constructor
func NewGitCommitView(dashboard *DashBoardController) *GitCommitView {
	gbv := new(GitCommitView)
	gbv.dashboard = dashboard
	gbv.git = gitbind.NewGitCaller().SetRepoPath(dashboard.GetConfig().Root().String("git.repo.path", ""))

	return gbv.initUI()
}

// Initialise user interface
func (gbv *GitCommitView) initUI() *GitCommitView {
	gbv.Box = crtview.NewBox()
	gbv.commitContent = crtview.NewTextView().SetDynamicColors(true)

	// Window
	layout := crtview.NewFlex().SetDirection(crtview.FlexRow)
	layout.AddItem(gbv.commitContent, 0, 1, false)

	gbv.window = crtview.NewWindow(layout).SetPositionCenter().SetFullscreen(true).SetMarginBorder(4, 7, 4, 7).Hide()

	gbv.commitContent.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {
		case tcell.KeyEsc:
			gbv.GetWindow().Hide()
			gbv.dashboard.PkgCntViewer.srcLogWindow.Show()
		}
		return event
	})

	return gbv
}

// LoadCommit by the specific SHA checksum
func (gbv *GitCommitView) LoadCommit(pkg *PkgFileInfo, checksum string) *GitCommitView {
	gbv.commitContent.Clear()
	gbv.alertError = nil
	gbv.window.SetTitle(fmt.Sprintf(" Path: %s/%s ", pkg.Package.Name(), pkg.PkgFileInfo.Name()))

	var commitData []string
	commitData, gbv.alertError = gbv.git.GetCommitData(fmt.Sprintf("%s/%s", pkg.Package.Name(), pkg.PkgFileInfo.Name()), checksum)
	if gbv.alertError != nil {
		return gbv
	}

	alc := "green"  // added line color
	rlc := "red"    // removed line color
	plc := "purple" // patch codelines
	pplc := "teal"  // patch header content
	dlc := "silver" // default color
	var content strings.Builder
	content.WriteString("[yellow]")
	for _, line := range commitData {
		if strings.HasPrefix(line, "-") {
			line = fmt.Sprintf("[%s]%s[%s]", rlc, line, dlc)
		} else if strings.HasPrefix(line, "+") {
			line = fmt.Sprintf("[%s]%s[%s]", alc, line, dlc)
		} else if strings.HasPrefix(line, "@@ ") {
			lp := strings.SplitN(line, "@@", 3)
			if len(lp) == 3 {
				line = fmt.Sprintf("[%s]@@ %s @@ [%s]%s[%s]", plc, lp[1], pplc, lp[2], dlc)
			}
		}
		content.WriteString(fmt.Sprintf("%s\n", line))
	}
	gbv.commitContent.SetText(content.String())

	return gbv
}

func (gbv *GitCommitView) Show() *GitCommitView {
	if gbv.alertError != nil {
		gbv.dashboard.PkgCntViewer.alertWindow.SetText("Error: " + gbv.alertError.Error()).Show()
	} else {
		gbv.dashboard.app.SetFocus(gbv.commitContent)
		gbv.GetWindow().Show()
	}
	return gbv
}

func (gbv *GitCommitView) GetWindow() *crtview.Window {
	return gbv.window
}
