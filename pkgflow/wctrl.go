package pkgflow

import (
	"sort"

	"github.com/cavaliercoder/go-rpm"
	"github.com/gdamore/tcell/v2"
	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	"github.com/isbm/crtview"
	"github.com/isbm/go-nanoconf"
	"gitlab.com/isbm/repodiff/pkgdiff"
	"gitlab.com/isbm/repodiff/pkgresolve"
	"gitlab.com/isbm/repodiff/reposcan"
)

type DashBoardController struct {
	RootContainer *crtview.Flex
	Filter        *crtview.DropDown
	PkgList       *PkgListTable
	PkgInfo       *PkgInfoWidget
	Pages         *crtview.Pages
	PkgCntViewer  *PkgContentViewer
	PkgDepViewer  *PkgDepView
	searchWindow  *SearchWindow
	helpPopup     *HelpWindow
	statusRibbon  *crtview.TextView

	ROOT_WINDOW    string
	CONTENT_VIEWER string
	DEP_VIEWER     string
	DIFF_RESOLVE   string
	SEARCH_POPUP   string
	HELP_POPUP     string

	term        *TerminalSize
	app         *crtview.Application
	resolver    *pkgresolve.PkgAcceptanceResolver
	diff        *reposcan.LayoutDiff
	currentMode string
	config      *nanoconf.Config

	wzlib_logger.WzLogger
}

func NewDashBoardController(app *crtview.Application, resolver *pkgresolve.PkgAcceptanceResolver, config *nanoconf.Config) *DashBoardController {
	dbc := new(DashBoardController)
	dbc.app = app
	dbc.resolver = resolver
	dbc.term = new(TerminalSize)

	dbc.currentMode = "all"
	dbc.ROOT_WINDOW = "root"
	dbc.CONTENT_VIEWER = "content_viewer"
	dbc.DEP_VIEWER = "dep_viewer"
	dbc.DIFF_RESOLVE = "diff_resolver"
	dbc.SEARCH_POPUP = "search_popup"
	dbc.HELP_POPUP = "help_popup"

	dbc.config = config
	dbc.initLayout()

	return dbc
}

func (dbc *DashBoardController) initLayout() {
	dbc.Filter = crtview.NewDropDown()
	dbc.Filter.SetLabel("Show ")
	dbc.Filter.SetOptions(nil,
		crtview.NewDropDownOption("All"),
		crtview.NewDropDownOption("To Review"),
		crtview.NewDropDownOption("Changed"),
		crtview.NewDropDownOption("Added"),
		crtview.NewDropDownOption("Deleted"),
		crtview.NewDropDownOption("Unchanged"),
	)
	dbc.Filter.ShowFocus(true)
	dbc.Filter.SetLabelColorFocused(tcell.ColorWhite)
	dbc.Filter.SetFieldBackgroundColor(tcell.ColorGreen)
	dbc.Filter.SetFieldBackgroundColorFocused(tcell.ColorWhite)
	dbc.Filter.SetFieldTextColor(tcell.ColorWhite)
	dbc.Filter.SetFieldTextColorFocused(tcell.ColorBlack)
	dbc.Filter.SetLabelColor(tcell.ColorGrey)
	dbc.Filter.SetSelectedFunc(dbc.onFilterSelected)
	dbc.Filter.SetInputCapture(dbc.onFilterKeypress)

	// Package lister
	dbc.PkgList = NewPkgListTable()
	dbc.PkgList.SetSelectedFunc(dbc.onPkgListSelected).
		SetSelectionChangedFunc(dbc.onPkgListSelectionChanged).
		SetInputCapture(dbc.onPkgListKeypress)

	// Package information display (r/o)
	dbc.PkgInfo = NewPkgInfoWidget()

	// Main window layout
	pkgListColumn := crtview.NewFlex()
	pkgListColumn.SetDirection(crtview.FlexRow)
	pkgListColumn.AddItem(dbc.Filter, 1, 0, false)
	pkgListColumn.AddItem(dbc.PkgList, 0, 1, false)

	dbc.statusRibbon = crtview.NewTextView().SetBackgroundColor(tcell.ColorDarkBlue).SetTextColor(tcell.ColorBlue).
		SetDynamicColors(true).SetText("[white]ENTER[blue] to select a package  [white]Ctrl-C[blue] to quit  [white]Ctrl-H[blue] for help")

	pkgListContainer := crtview.NewFlex()
	pkgListContainer.AddItem(pkgListColumn, 0, 1, false)
	pkgListContainer.AddItem(dbc.PkgInfo.GetContainer(), 0, 1, false)

	dbc.RootContainer = crtview.NewFlex().SetDirection(crtview.FlexRow).
		AddItem(pkgListContainer, 0, 1, false).
		AddItem(dbc.statusRibbon, 1, 0, false)

	dbc.PkgCntViewer = NewPkgContentViewer(dbc)
	dbc.PkgDepViewer = NewPkgDepView(dbc)
	dbc.searchWindow = NewSearchWindow(dbc)
	dbc.helpPopup = NewHelpWindow(dbc)

	dbc.Pages = crtview.NewPages()
	dbc.Pages.AddPage(dbc.ROOT_WINDOW, dbc.RootContainer, true, true)
	dbc.Pages.AddPage(dbc.CONTENT_VIEWER, dbc.PkgCntViewer, true, false)
	dbc.Pages.AddPage(dbc.DEP_VIEWER, dbc.PkgDepViewer, true, false)
	dbc.Pages.AddPage(dbc.SEARCH_POPUP, dbc.searchWindow, true, false)
	dbc.Pages.AddPage(dbc.HELP_POPUP, dbc.helpPopup, true, false)
}

// GetConfig
func (dbc *DashBoardController) GetConfig() *nanoconf.Config {
	return dbc.config
}

func (dbc *DashBoardController) inspectPkgStatus(pkg *reposcan.PackageMeta) *reposcan.PackageMeta {
	pkgstat := dbc.resolver.CheckPackage(pkg.Package.Name())
	if pkg.Status == reposcan.STATUS_NEW || pkg.Status == reposcan.STATUS_REMOVED {
		for _, nfo := range pkg.Package.Files() {
			if pkgstat.GetFile(nfo.Name()).GetStatus() != pkgresolve.STATUS_ACCEPTED {
				return pkg
			}
		}
		pkg.Status = reposcan.STATUS_UNCHANGED
	}
	return pkg
}

func (dbc *DashBoardController) inspectDiffStatus(pkgdiff *pkgdiff.UpdateDiff) *reposcan.PackageMeta {
	meta := new(reposcan.PackageMeta)
	meta.Package = pkgdiff.Package
	meta.Status = reposcan.STATUS_UNCHANGED

	rpkg := dbc.resolver.CheckPackage(meta.Package.Name())

	// TODO: Take care of deleted files? But for this GUI needs to present them in a meaningful way --+
	//                                                                                                |
	//                                                                                                V
	for _, diffset := range [][]rpm.FileInfo{pkgdiff.ChangedFiles, pkgdiff.NewFiles} { //, pkgdiff.DeletedFiles} {
		for _, f := range diffset {
			if rpkg.CheckFile(f.Name()).GetStatus() != pkgresolve.STATUS_ACCEPTED {
				meta.Status = reposcan.STATUS_UPDATED
				return meta
			}
		}
	}

	return meta
}

func (dbc *DashBoardController) getAllDiffPackages() []*reposcan.PackageMeta {
	pkgs := make([]*reposcan.PackageMeta, 0)

	for _, pslice := range [][]*reposcan.PackageMeta{dbc.diff.NewPackages, dbc.diff.RemovedPackages, dbc.diff.UnchangedPackages} {
		for _, pkg := range pslice {
			pkgs = append(pkgs, dbc.inspectPkgStatus(pkg))
		}
	}

	for _, pkgdiff := range dbc.diff.ChangedPackages {
		pkgs = append(pkgs, dbc.inspectDiffStatus(pkgdiff))
	}

	return pkgs
}

func (dbc *DashBoardController) Refresh() *DashBoardController {
	dbc.loadPackageList()
	return dbc
}

func (dbc *DashBoardController) SetPackageListMode(mode string) *DashBoardController {
	dbc.currentMode = mode
	return dbc
}

func (dbc *DashBoardController) loadPackageList() {
	dbc.PkgList.FlushContent()

	if dbc.diff == nil {
		dbc.app.Stop()
		dbc.GetLogger().Error("No packages has been loaded yet")
	} else {
		switch dbc.currentMode {
		case "all":
			dbc.propagatePackageList([]int{-1})
		case "to review":
			dbc.propagatePackageList([]int{reposcan.STATUS_NEW, reposcan.STATUS_UPDATED, reposcan.STATUS_REMOVED})
		case "changed":
			dbc.propagatePackageList([]int{reposcan.STATUS_UPDATED})
		case "unchanged":
			dbc.propagatePackageList([]int{reposcan.STATUS_UNCHANGED})
		case "deleted":
			dbc.propagatePackageList([]int{reposcan.STATUS_REMOVED})
		case "added":
			dbc.propagatePackageList([]int{reposcan.STATUS_NEW})
		}
	}
}

func (dbc *DashBoardController) propagatePackageList(status []int) *DashBoardController {
	pkgs := make(map[string]*reposcan.PackageMeta)
	pkgsIdx := make([]string, 0)

	for _, pkg := range dbc.getAllDiffPackages() {
		for _, s := range status {
			if pkg.Status == s || s == -1 {
				pkgs[pkg.Package.Name()] = pkg
				pkgsIdx = append(pkgsIdx, pkg.Package.Name())
				break
			}
		}
	}
	sort.Strings(pkgsIdx)
	for idx, name := range pkgsIdx {
		dbc.PkgList.AddPackageInfo(idx, pkgs[name])
	}
	return dbc
}
