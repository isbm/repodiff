package pkgresolve

import (
	"strings"
)

const (
	STATUS_UNKNOWN = iota
	STATUS_ACCEPTED
	STATUS_REJECTED
	STATUS_IN_REVIEW // unused yet
)

type FileStatus struct {
	status  int
	comment string
}

func NewFileStatus() *FileStatus {
	fs := new(FileStatus)
	return fs
}

// SetComment optionally to the decided action
func (fs *FileStatus) SetComment(text string) *FileStatus {
	fs.comment = strings.ReplaceAll(text, "\n", " ")
	return fs
}

// Accept the change of the file
func (fs *FileStatus) Accept() *FileStatus {
	fs.status = STATUS_ACCEPTED
	return fs
}

// Reject the change of the file
func (fs *FileStatus) Reject() *FileStatus {
	fs.status = STATUS_REJECTED
	return fs
}

// Review the change of the file
func (fs *FileStatus) Review() *FileStatus {
	fs.status = STATUS_IN_REVIEW
	return fs
}

// Reset the change of the file
func (fs *FileStatus) Reset() *FileStatus {
	fs.status = STATUS_UNKNOWN
	fs.comment = ""
	return fs
}

// GetStatus of the file
func (fs *FileStatus) GetStatus() int {
	return fs.status
}

// GetComment of the file
func (fs *FileStatus) GetComment() string {
	return fs.comment
}
