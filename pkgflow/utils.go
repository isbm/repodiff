package pkgflow

import (
	"mime"
	"path/filepath"
	"strings"

	gomime "github.com/cubewise-code/go-mime"
)

// FileMimeType to determine Content Type by file
// extension without actually reading it just yet
type FileMimeType struct {
	overlay []string
}

func NewFileMimeType() *FileMimeType {
	mmt := new(FileMimeType)
	mmt.overlay = []string{
		".spec",
		".changes",
		".service",
		".target",
		".sysconfig",
	}
	return mmt
}

func (mmt *FileMimeType) MimeTypeByFilename(name string) string {
	ext := filepath.Ext(name)
	for _, ovlExt := range mmt.overlay {
		if ext == ovlExt {
			return "text/plain"
		}
	}
	ct := mime.TypeByExtension(ext)
	if ct == "" {
		ct = gomime.TypeByExtension(ext)
	}

	return strings.Split(ct, ";")[0]
}
