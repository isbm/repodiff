#
# spec file for package eb-repodiff
#
# Copyright (c) 2020 Elektrobit Automotive, Erlangen, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

Name:           eb-repodiff
Version:        0.1
Release:        0
Summary:        EB repository difference scanner
License:        MIT
Group:          Automotive/Tools
Url:            https://gitlab.com/isbm/repodiff
Source:         %{name}-%{version}.tar.gz
Source1:        vendor.tar.gz
BuildRequires:  git-core
BuildRequires:  golang-packaging
BuildRequires:  golang(API) >= 1.13
%{?systemd_ordering}

%description
Scans two repositories for a differences, providing a Git-integrated workflow for difference management and tracking.

%prep
%setup -q
%setup -q -T -D -a 1

%build
# Output for log purpuses
go env

# Build the binary
go build -x -mod=vendor -buildmode=pie -o %{name} ./cmd/repodiff-cli.go
ls -lah

%install
install -D -m 0755 %{name} %{buildroot}%{_bindir}/%{name}
mkdir -p %{buildroot}%{_sysconfdir}
install -m 0644 ./cmd/repodiff.conf.example %{buildroot}%{_sysconfdir}/repodiff.conf

%files
%defattr(-,root,root)
%{_bindir}/%{name}
%dir %{_sysconfdir}
%config /etc/repodiff.conf

%changelog
