module gitlab.com/isbm/repodiff

go 1.15

//replace github.com/isbm/crtview => /home/bo/work/crtview

require (
	github.com/antonfisher/nested-logrus-formatter v1.3.0 // indirect
	github.com/cavaliercoder/badio v0.0.0-20160213150051-ce5280129e9e // indirect
	github.com/cavaliercoder/go-rpm v0.0.0-20200122174316-8cb9fd9c31a8
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/cubewise-code/go-mime v0.0.0-20200519001935-8c5762b177d8
	github.com/dustin/go-humanize v1.0.0
	github.com/gdamore/tcell/v2 v2.1.0
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/google/go-cmp v0.5.3 // indirect
	github.com/infra-whizz/wzlib v0.0.0-20201210130450-2b56d9cf0495
	github.com/isbm/crtview v1.6.2
	github.com/isbm/go-nanoconf v0.0.0-20200623180822-caf90de1965e
	github.com/isbm/textwrap v0.0.0-20190729202254-22edad10bd84
	github.com/karrick/godirwalk v1.16.1
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/urfave/cli/v2 v2.3.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	gotest.tools v2.2.0+incompatible // indirect
)
