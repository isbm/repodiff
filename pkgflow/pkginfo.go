package pkgflow

import (
	"fmt"
	"path"
	"strings"

	"github.com/gdamore/tcell/v2"
	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	"gitlab.com/isbm/repodiff/reposcan"
	"github.com/isbm/crtview"
)

type PkgInfoWidget struct {
	loaded         bool
	pkgInfo        *crtview.Table
	pkgDescription *crtview.TextView
	container      *crtview.Flex
	wzlib_logger.WzLogger
}

func NewPkgInfoWidget() *PkgInfoWidget {
	piw := new(PkgInfoWidget)

	piw.pkgInfo = crtview.NewTable()
	piw.pkgInfo.SetFixed(1, 1)
	piw.pkgInfo.SetBorder(true)
	piw.pkgInfo.SetTitle("(Select package)")
	piw.pkgInfo.SetBorderColor(tcell.ColorDefault)
	piw.pkgInfo.SetTitleAlign(crtview.AlignLeft)
	piw.pkgInfo.SetSelectable(false, false)

	piw.pkgDescription = crtview.NewTextView()
	piw.pkgDescription.SetBorder(true)
	piw.pkgDescription.SetBorderColor(tcell.ColorDefault)

	piw.container = crtview.NewFlex()
	piw.container.SetDirection(crtview.FlexRow)
	piw.container.AddItem(piw.pkgInfo, 0, 1, false)
	piw.container.AddItem(piw.pkgDescription, 0, 1, false)

	return piw
}

func (piw *PkgInfoWidget) GetContainer() *crtview.Flex {
	return piw.container
}

// Get label cell
func (piw *PkgInfoWidget) toLabel(text string) *crtview.TableCell {
	lbl := crtview.NewTableCell(strings.ToUpper(string(text[0])) + strings.ToLower(text[1:]) + ":")
	lbl.SetTextColor(tcell.ColorDarkGray)
	lbl.SetAlign(crtview.AlignRight)
	return lbl
}

// Get field cell
func (piw *PkgInfoWidget) toField(text string) *crtview.TableCell {
	vl := crtview.NewTableCell(text)
	vl.SetTextColor(tcell.ColorLightGray)
	vl.SetAlign(crtview.AlignLeft)

	return vl
}

// IsLoaded
func (piw *PkgInfoWidget) IsLoaded() bool {
	return piw.loaded
}

func (piw *PkgInfoWidget) LoadPackageInfo(pkg *reposcan.PackageMeta) {
	piw.loaded = true
	piw.pkgInfo.Clear()
	piw.pkgInfo.SetTitleColor(tcell.ColorYellow)
	piw.pkgInfo.SetTitle(fmt.Sprintf(" Info about \"%s\" package ", pkg.Package.Name()))

	infoset := [][]string{
		[]string{"summary", pkg.Package.Summary()},
		[]string{"file name", path.Base(pkg.Package.Path())},
		[]string{"version", pkg.Package.Version()},
		[]string{"licence", pkg.Package.License()},
		[]string{"built", fmt.Sprintf("%s on %s", pkg.Package.BuildHost(), pkg.Package.BuildTime())},
	}

	// Platform
	if pkg.Package.Platform() != "" {
		infoset = append(infoset, []string{"platform", pkg.Package.Platform()})
	}

	// Build
	if pkg.Package.Architecture() != "" {
		infoset = append(infoset, []string{"architecture", pkg.Package.Architecture()})
	}

	// Packager
	if pkg.Package.Packager() != "" {
		infoset = append(infoset, []string{"packager", pkg.Package.Packager()})
	}

	// Vendor
	if pkg.Package.Vendor() != "" {
		infoset = append(infoset, []string{"vendor", pkg.Package.Vendor()})
	}

	// Checksum
	checksum, err := pkg.Package.Checksum()
	if err != nil {
		piw.GetLogger().Errorf("Unable to get checksum type from package %s: %s", pkg.Package.Name(), err.Error())
	}
	infoset = append(infoset, []string{fmt.Sprintf("Checksum (%s)", pkg.Package.ChecksumType()), checksum})
	for offset, kp := range infoset {
		label, value := kp[0], kp[1]
		piw.pkgInfo.SetCell(offset, 0, piw.toLabel(label))
		piw.pkgInfo.SetCell(offset, 1, piw.toField(value))
	}

	piw.pkgDescription.SetTitle(" Description ")
	piw.pkgDescription.SetTitleColor(tcell.ColorYellow)
	piw.pkgDescription.SetTitleAlign(crtview.AlignLeft)
	piw.pkgDescription.SetText(pkg.Package.Description())
	piw.pkgDescription.SetTextColor(tcell.ColorDefault)
}
