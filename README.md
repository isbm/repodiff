# repodiff - repository difference tool

A tool that allows to compare two repositories at package source level to find package changes at its content and log each audit step with.

## Concept

Changes between updates are natural, but how do we know which changes do we want to accept in a mission-critical system and which not? This tool enables to have a Git-based audit workflow. Essentially, it creates an overlay file, where changes are registered as known, one of accepted, rejected or postponed (in review/on hold). And the more known (accepted) changes are in this overlay, the less reporting of different packages is reported. Once overlay is registered with all changes and all of them are accepted, no difference between the repositories will be shown.

The overlay file is a simple YAML file with the following format:

```yaml
package-name:
    file-name:
        status: accepted  # rejected or review
        comment: Free text, explaining the reason of the chosen status
```

An example would be:

```yaml
bash:
    bash.spec:
        status: accepted
        comment: A necessary change to update an environment variables
```

Once this entry is created, you can manage these changes in your own Git repo to track down each step.

## Usage

To compare repositories, the `repodiff` can output a simple difference, returning a list of changed/new/removed packages, or provide a user interface, that would allow you to make detailed review of each change.

### Download Repositories

Before anything, you need to have a two repositories of source RPM packages (source Deb packages are not supported just yet):
  - Base repository, currently used. Assuming it is downloaded to `/opt/repos/current`
  - Update repository with the new packages. Assuming it is downloaded to `/opt/repos/update`

### Setup Audit Git Repo

Each audit step of acceptance or rejection or postponing any change between the package repositories is logged and integrated with the Git repository. All the comparison should be done inside that repository. Check out one or create from scratch. Assuming there is no such repository yet, create one, e.g. in `/opt/audit` and add overlay file (default name is `release-acceptance.yaml`):

    mkdir /opt/audit
    cd /opt/audit
    git init
    touch release-acceptance.yaml
    git add release-acceptance.yaml
    git commit -am "initial state"

Later on you can push it anywhere you like.

### Get a List of Different Packages

In the `/opt/audit` you should be able to find out which packages has been changed to obtain a simple list of those:

    repodiff -b /opt/repos/current -u /opt/repos/update -f

In case there are any packages that are different, the return code will be non-zero (useful for scripting).

## Package Difference Workflow

### Start UI

To start a terminal user interface that shows you all the details of each package, use `-w` flag instead of `-f` above:

    repodiff -b /opt/repos/current -u /opt/repos/update -w

This will do two things for you at one time:

- Create a new branch in Git repository for you (as long as it starts from `master` branch)
- Start a terminal based UI.
    
The UI has a screen, divided vertically. Left column is all the packages, colored differently depending on their status, right column has overall info about the package and its description below. Hit `Ctrl+H` for help. Place a cursor on the package of your interest and hit ENTER to see its content. In order to let package disappear from "To Review" (see below), you must to accept all the changes. If any of the changes are rejected or in review, this means that this package is not yet finished and cannot be installed, as you are obviously not yet happy with the final outcome.

Every time you add/reject/hold a change in a package (or accept the entire new package), it will add a separate commit to the overlay file. To see all the changes to the overlay file, type `git log` or use `tig` terminal UI for Git (installed separately on your own).

Once you've finished your reviews, push your auditing Git repo to the remote as PR for your team review. Congratulations!

### Limit To Only Changed Packages

To limit the repository to only changed packages, navigate to the first package on top (Arrow Left) and then Arrow Up to to select a drop-down that is by default shows all packages. Press ENTER and select "To Review".
