package pkgresolve

import (
	"sort"
)

// ResolvedPackage type
type ResolvedPackage struct {
	name      string
	fileIndex []string
	files     map[string]*FileStatus
}

func NewResolvedPackage(name string) *ResolvedPackage {
	rp := new(ResolvedPackage)
	rp.name = name
	rp.fileIndex = make([]string, 0)
	rp.files = make(map[string]*FileStatus)
	return rp
}

// Name of the package
func (rp *ResolvedPackage) Name() string {
	return rp.name
}

// IsEmpty returns true if there is no any file resolutions registered
func (rp *ResolvedPackage) IsEmpty() bool {
	return len(rp.fileIndex) == 0
}

// GetFile returns an existing record or transparently creates new.
func (rp *ResolvedPackage) GetFile(name string) *FileStatus {
	fstat := rp.files[name]
	if fstat == nil {
		fstat = NewFileStatus()
		rp.files[name] = fstat
		rp.fileIndex = append(rp.fileIndex, name)
		sort.Strings(rp.fileIndex)
	}

	return fstat
}

// CheckFile for an existing record of its status
func (rp *ResolvedPackage) CheckFile(name string) *FileStatus {
	fstat := rp.files[name]
	if fstat == nil {
		fstat = NewFileStatus()
	}
	return fstat
}
