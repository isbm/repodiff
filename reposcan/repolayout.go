package reposcan

import (
	"github.com/cavaliercoder/go-rpm"
	wzlib_logger "github.com/infra-whizz/wzlib/logger"
)

// Repository layout diff. Collects information about newly added or removed packages.

// RepoLayoutDiff type
type RepoLayoutDiff struct {
	repoType   string
	repodiff   *LayoutDiff
	baseRPMs   map[string]*rpm.PackageFile
	updateRPMs map[string]*rpm.PackageFile

	wzlib_logger.WzLogger
}

// NewRepoLayoutDiff is RepoLayoutDiff constructor function
func NewRepoLayoutDiff(repotype string) *RepoLayoutDiff {
	rld := new(RepoLayoutDiff)
	rld.repoType = repotype
	rld.repodiff = NewLayoutDiff()
	rld.baseRPMs = make(map[string]*rpm.PackageFile)
	rld.updateRPMs = make(map[string]*rpm.PackageFile)

	return rld
}

// SetBaseRepo path to the current base repository
func (rld *RepoLayoutDiff) SetBaseRepo(pth string) *RepoLayoutDiff {
	rld.repodiff.BaseRepoPath = pth
	return rld
}

// SetUpdateRepo path to the next repository
func (rld *RepoLayoutDiff) SetUpdateRepo(pth string) *RepoLayoutDiff {
	rld.repodiff.UpdateRepoPath = pth
	return rld
}

// Diff two repositories by package names
func (rld *RepoLayoutDiff) Diff() (*LayoutDiff, error) {
	if err := rld.scanBaseRepo(); err != nil {
		return nil, err
	}

	/*
		fmt.Println("\nNew packages:")
		for idx, pkg := range rld.repodiff.NewPackages {
			fmt.Printf("%d. %s\n", idx+1, pkg.Name())
		}

		fmt.Println("\nRemoved packages:")
		for idx, pkg := range rld.repodiff.RemovedPackages {
			fmt.Printf("%d. %s\n", idx+1, pkg.Name())
		}

		fmt.Println("\nChanged packages:")
		for idx, pkg := range rld.repodiff.ChangedPackages {
			fmt.Printf("Package: %d. %s (base: %s, at %s)\n", idx+1, pkg.Package.Name(), pkg.Package.Version(), pkg.Package.Path())

			if len(pkg.ChangedFiles) > 0 {
				fmt.Println("  Changed files:")
				for cidx, cfile := range pkg.ChangedFiles {
					fmt.Printf("    %d. %s\n", cidx+1, cfile.Name())
				}
			}

			if len(pkg.NewFiles) > 0 {
				fmt.Println("  New files:")
				for nidx, nfile := range pkg.NewFiles {
					fmt.Printf("    %d. %s\n", nidx+1, nfile.Name())
				}
			}

			if len(pkg.DeletedFiles) > 0 {
				fmt.Println("  Removed files:")
				for ridx, rfile := range pkg.DeletedFiles {
					fmt.Printf("    %d. %s\n", ridx+1, rfile.Name())
				}
			}
			fmt.Println("-----")
		}
	*/

	return rld.repodiff, nil
}
