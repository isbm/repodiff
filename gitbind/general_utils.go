package gitbind

import (
	"os"
	"strconv"
	"sync"
	"time"

	wzlib_subprocess "github.com/infra-whizz/wzlib/subprocess"
)

var mux sync.Mutex
var rnd uint32

// RandomString generates a... random string :)
func RandomString() string {
	mux.Lock()
	r := rnd
	if r == 0 {
		r = uint32(time.Now().UnixNano() + int64(os.Getpid()))
	}
	r = r*1664525 + 1013904223
	rnd = r
	mux.Unlock()
	return strconv.Itoa(int(1e9 + r%1e9))[1:]
}

// Add to the sequence the whole changelog entry and reset WIP status
func CallWithTTY(name string, args ...string) error {
	cmd := wzlib_subprocess.ExecCommand(name, args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	err := cmd.Run()
	if err != nil {
		return err
	}
	return nil
}
