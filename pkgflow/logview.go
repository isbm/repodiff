package pkgflow

import (
	"fmt"

	"github.com/gdamore/tcell/v2"
	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	"github.com/isbm/crtview"
	"gitlab.com/isbm/repodiff/gitbind"
)

// GitLogView class
type GitLogView struct {
	git        *gitbind.GitCaller
	srcLogList *crtview.Table
	statusView *crtview.TextView
	dashboard  *DashBoardController
	window     *crtview.Window
	srcLog     []*gitbind.GSCLogItem
	currentPkg *PkgFileInfo
	branchName string
	alertError error

	*crtview.Box
	wzlib_logger.WzLogger
}

// NewGitLogView constructor
func NewGitLogView(dashboard *DashBoardController) *GitLogView {
	gbv := new(GitLogView)
	gbv.dashboard = dashboard
	gbv.git = gitbind.NewGitCaller().SetRepoPath(dashboard.GetConfig().Root().String("git.repo.path", ""))

	return gbv.initUI()
}

// Initialise user interface
func (gbv *GitLogView) initUI() *GitLogView {
	gbv.Box = crtview.NewBox()
	gbv.statusView = crtview.NewTextView().SetTextAlign(crtview.AlignLeft).
		SetBackgroundColor(tcell.ColorBlue).SetTextColor(tcell.ColorWhite).
		SetDynamicColors(true)
	gbv.srcLogList = crtview.NewTable().SetSelectable(true, false).SetSelectedStyle(tcell.ColorWhite, tcell.ColorGreen, tcell.AttrBold)
	gbv.srcLogList.SetSelectionChangedFunc(func(row int, column int) {
		item := gbv.srcLog[row]
		gbv.statusView.SetText(fmt.Sprintf(" [aqua::b][%s] [white]%s [aqua::-]- commit %d of %d", gbv.branchName, item.Commit, row+1, len(gbv.srcLog)))
	})

	// Window
	layout := crtview.NewFlex().SetDirection(crtview.FlexRow).
		AddItem(gbv.srcLogList, 0, 1, false).
		AddItem(gbv.statusView, 1, 1, false)
	gbv.window = crtview.NewWindow(layout).SetPositionCenter().SetFullscreen(true).SetMarginBorder(4, 7, 4, 7).Hide()

	gbv.srcLogList.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {
		case tcell.KeyEsc:
			gbv.GetWindow().Hide()
			gbv.dashboard.app.SetFocus(gbv.dashboard.PkgCntViewer.fileList)
		case tcell.KeyEnter:
			gbv.LoadCurrentCommit()
			return nil
		}
		return event
	})
	return gbv
}

func (gbv *GitLogView) LoadCurrentCommit() {
	r, _ := gbv.srcLogList.GetSelection()
	item := gbv.srcLog[r]
	gbv.dashboard.PkgCntViewer.commitViewWindow.LoadCommit(gbv.currentPkg, item.Commit)
	gbv.GetWindow().Hide()
	gbv.dashboard.PkgCntViewer.commitViewWindow.Show()
}

// Get a log on a file within the Git repo. The current chdir
// should be within that Git repo.
func (gbv *GitLogView) LoadLog(pkg *PkgFileInfo) *GitLogView {
	gbv.srcLogList.Clear()
	gbv.alertError = nil
	gbv.currentPkg = pkg
	gbv.window.SetTitle(fmt.Sprintf(" Path: %s/%s ", pkg.Package.Name(), pkg.PkgFileInfo.Name()))

	gbv.branchName = gbv.git.GetCurrentBranch()
	gbv.srcLog, gbv.alertError = gbv.git.GetLog(fmt.Sprintf("%s/%s", pkg.Package.Name(), pkg.PkgFileInfo.Name()))
	if gbv.alertError != nil {
		return gbv
	}

	for idx, logItem := range gbv.srcLog {
		gbv.srcLogList.SetCell(idx, 0, crtview.NewTableCell(logItem.GetFormattedDate()).SetTextColor(tcell.ColorBlue))
		gbv.srcLogList.SetCell(idx, 1, crtview.NewTableCell(logItem.Author.Name).SetTextColor(tcell.ColorGreen))
		gbv.srcLogList.SetCell(idx, 2, crtview.NewTableCell(logItem.GetShortMessage()).SetExpansion(1).SetTextColor(tcell.ColorWhite))
	}

	if len(gbv.srcLog) > 0 {
		gbv.srcLogList.Select(0, 0)
	}

	return gbv
}

func (gbv *GitLogView) Show() *GitLogView {
	if gbv.alertError != nil {
		gbv.dashboard.PkgCntViewer.alertWindow.SetText("Error: " + gbv.alertError.Error()).Show()
	} else {
		gbv.dashboard.app.SetFocus(gbv.srcLogList)
		gbv.GetWindow().Show()
	}
	return gbv
}

func (gbv *GitLogView) GetWindow() *crtview.Window {
	return gbv.window
}
