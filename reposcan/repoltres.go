package reposcan

import (
	"github.com/cavaliercoder/go-rpm"
	"gitlab.com/isbm/repodiff/pkgdiff"
)

const (
	STATUS_UNCHANGED = iota
	STATUS_NEW
	STATUS_UPDATED
	STATUS_REMOVED
)

type PackageMeta struct {
	Package *rpm.PackageFile
	Status  int
}

// LayoutDiff type
type LayoutDiff struct {
	BaseRepoPath   string
	UpdateRepoPath string

	UnchangedPackages []*PackageMeta
	NewPackages       []*PackageMeta
	RemovedPackages   []*PackageMeta
	ChangedPackages   []*pkgdiff.UpdateDiff
}

func NewLayoutDiff() *LayoutDiff {
	ld := new(LayoutDiff)
	ld.NewPackages, ld.RemovedPackages, ld.UnchangedPackages = make([]*PackageMeta, 0), make([]*PackageMeta, 0), make([]*PackageMeta, 0)
	ld.ChangedPackages = make([]*pkgdiff.UpdateDiff, 0)

	return ld
}
