package pkgflow

import (
	"math/big"

	"github.com/dustin/go-humanize"
	"github.com/gdamore/tcell/v2"
	"github.com/isbm/crtview"
	"gitlab.com/isbm/repodiff/reposcan"
)

type PkgListTable struct {
	packageIndex  map[string]*reposcan.PackageMeta
	packageOffset map[string]int
	crtview.Table
}

func NewPkgListTable() *PkgListTable {
	pkl := new(PkgListTable)

	pkl.Box = crtview.NewBox()
	pkl.SetScrollBarVisibility(crtview.ScrollBarAuto).SetScrollBarColor(crtview.Styles.ScrollBarColor).
		SetBordersColor(crtview.Styles.GraphicsColor).SetSeparator(' ').SetSortClicked(true).
		SetFixed(1, 1).SetBorders(false).SetSelectable(true, false).
		SetSelectedStyle(tcell.ColorWhite, tcell.ColorDarkGreen, tcell.AttrBold).SetEvaluateAllRows(true)

	pkl.FlushContent()

	return pkl
}

// FlushContent of the table list, reinit the header and reset the selection policies
func (pkl *PkgListTable) FlushContent() {
	pkl.Clear()
	pkl.packageIndex = make(map[string]*reposcan.PackageMeta)
	pkl.packageOffset = make(map[string]int)
	headerCell := func(label string) *crtview.TableCell {
		return crtview.NewTableCell(label).SetSelectable(false).SetBackgroundColor(tcell.ColorBlue).
			SetAttributes(tcell.AttrBold).SetTextColor(tcell.ColorWhite)
	}
	for offset, label := range []string{"Name", "Version", "Size"} {
		cell := headerCell(label)
		if offset == 1 {
			cell.SetAlign(crtview.AlignRight)
			cell.Text = []byte(string(cell.Text) + " ")
		}
		pkl.SetCell(0, offset, cell)
	}
}

// Add one package row
func (pkl *PkgListTable) AddPackageInfo(offset int, pkg *reposcan.PackageMeta) {
	offset += 1
	// Name
	name := crtview.NewTableCell(pkg.Package.Name()).SetAlign(crtview.AlignLeft).SetExpansion(1).SetSelectable(true)
	switch pkg.Status {
	case reposcan.STATUS_UPDATED:
		name.SetTextColor(tcell.ColorOrange.TrueColor())
	case reposcan.STATUS_REMOVED:
		name.SetTextColor(tcell.ColorDimGray.TrueColor())
	case reposcan.STATUS_NEW:
		name.SetTextColor(tcell.ColorRed.TrueColor())
	default:
		name.SetTextColor(tcell.ColorWhite.TrueColor())
	}

	pkl.SetCell(offset, 0, name)
	pkl.SetCell(offset, 1, crtview.NewTableCell(pkg.Package.Version()+" ").
		SetAlign(crtview.AlignRight).SetTextColor(tcell.ColorDefault).SetSelectable(true))
	pkl.SetCell(offset, 2, crtview.NewTableCell(humanize.BigBytes((&big.Int{}).SetUint64(pkg.Package.Size()))).
		SetTextColor(tcell.ColorDefault).SetSelectable(true))

	pkl.packageIndex[pkg.Package.Name()] = pkg
	pkl.packageOffset[pkg.Package.Name()] = offset
}

// GetPackageMetaByName returns reposcan.PackageMeta object, looked up by the package name
// in the list of the table, even if the row is not visible in the viewport.
func (pkl *PkgListTable) GetPackageMetaByName(name string) *reposcan.PackageMeta {
	obj, ex := pkl.packageIndex[name]
	if !ex {
		return nil
	}
	return obj
}

func (pkl *PkgListTable) GetPackageIndex() map[string]*reposcan.PackageMeta {
	return pkl.packageIndex
}

func (pkl *PkgListTable) GetPackageOffsetByName(name string) int {
	idx, ex := pkl.packageOffset[name]
	if !ex {
		return -1
	}
	return idx
}

// GetSelectedPackageMeta returns reposcan.PackageMeta object by selected name in the table
func (pkl *PkgListTable) GetSelectedPackageMeta() *reposcan.PackageMeta {
	r, _ := pkl.GetSelection()
	if r < 0 {
		r = 0
	}
	return pkl.GetPackageMetaByName(string(pkl.GetCell(r, 0).Text))
}
