package pkgflow

import (
	"fmt"
	"math/big"
	"strings"

	"github.com/cavaliercoder/go-rpm"
	"github.com/dustin/go-humanize"
	"github.com/gdamore/tcell/v2"
	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	"github.com/isbm/crtview"
	"gitlab.com/isbm/repodiff/pkgdiff"
	"gitlab.com/isbm/repodiff/pkgresolve"
	"gitlab.com/isbm/repodiff/reposcan"
)

type PkgFileInfo struct {
	Package        *rpm.PackageFile
	PkgFileInfo    rpm.FileInfo
	NeedResolution bool
}

// PkgContentViewer implements ncurses window to look into the content of the package
type PkgContentViewer struct {
	*crtview.Box

	dashboard        *DashBoardController
	wm               *crtview.WindowManager
	fileListHeader   *crtview.Table
	fileList         *crtview.Table
	window           *crtview.Window
	choiceWindow     *PkgDiffResolve
	pkgmeta          *reposcan.PackageMeta
	diffmeta         *pkgdiff.UpdateDiff
	fileMime         *FileMimeType
	viewHelp         *crtview.TextView
	blameWindow      *GitBlameView
	srcLogWindow     *GitLogView
	commitViewWindow *GitCommitView
	alertWindow      *AlertWindow

	bgColor         tcell.Color
	fgColor         tcell.Color
	arAcceptedColor tcell.Color
	arInReviewColor tcell.Color
	arRejectedColor tcell.Color

	fileIndex map[int]*PkgFileInfo

	wzlib_logger.WzLogger
}

// NewPkgContentViewer implements content viewer object constructor
func NewPkgContentViewer(dashboard *DashBoardController) *PkgContentViewer {
	pcv := new(PkgContentViewer)
	pcv.dashboard = dashboard
	pcv.fileMime = NewFileMimeType()

	// Initial colors (8)
	pcv.bgColor = tcell.ColorBlue
	pcv.fgColor = tcell.ColorLightCyan
	pcv.arAcceptedColor = tcell.ColorLime
	pcv.arInReviewColor = tcell.ColorYellow
	pcv.arRejectedColor = tcell.ColorRed

	pcv.Box = crtview.NewBox()
	pcv.fileListHeader = crtview.NewTable().SetSelectable(false, false).SetBorders(false).SetBackgroundColor(pcv.bgColor)
	pcv.fileList = crtview.NewTable().
		SetSelectable(true, false).
		SetBorders(false).
		SetSelectedStyle(tcell.ColorBlack, tcell.ColorTeal, tcell.AttrBold).
		SetBackgroundColor(pcv.bgColor).
		Sort(0, true).
		SetInputCapture(func(e *tcell.EventKey) *tcell.EventKey {
			switch e.Key() {
			case tcell.KeyEnter:
				pcv.ResolveDifference()
			case tcell.KeyESC:
				pcv.CloseWindow()
				return nil
			case tcell.KeyPgDn, tcell.KeyEnd:
				pcv.fileList.Select(len(pcv.fileIndex)-1, 0)
				return nil
			case tcell.KeyCtrlB:
				pcv.blameContentFile()
				return nil
			case tcell.KeyCtrlL:
				pcv.logContentFile()
				return nil
			}
			return e
		})

	pcv.initUI()
	pcv.fillPackageContents() // header only

	return pcv
}

func (pcv *PkgContentViewer) blameContentFile() {
	pkgMetaId, _ := pcv.fileList.GetSelection()
	pcv.blameWindow.Blame(pcv.fileIndex[pkgMetaId]).Show()
}

func (pcv *PkgContentViewer) logContentFile() {
	pkgMetaId, _ := pcv.fileList.GetSelection()
	pcv.srcLogWindow.LoadLog(pcv.fileIndex[pkgMetaId]).Show()
}

func (pcv *PkgContentViewer) ResolveDifference() {
	if pcv.pkgmeta.Status == reposcan.STATUS_UPDATED {
		pkgMetaId, _ := pcv.fileList.GetSelection()
		diffResolve := pcv.fileIndex[pkgMetaId]
		if diffResolve.NeedResolution {
			pcv.fileList.SetSelectable(false, false)
			row, _ := pcv.fileList.GetSelection()
			pcv.choiceWindow.SetLastSelected(row)
			pcv.choiceWindow.ResolveFile(diffResolve)
		}
	} else if pcv.pkgmeta.Status == reposcan.STATUS_NEW || pcv.pkgmeta.Status == reposcan.STATUS_REMOVED {
		pcv.fileList.SetSelectable(false, false)

		row, _ := pcv.fileList.GetSelection()
		pcv.choiceWindow.SetLastSelected(row)
		pcv.choiceWindow.ResolvePackage(pcv.pkgmeta)
	}
}

func (pcv *PkgContentViewer) CloseWindow() {
	pcv.fileList.SetSelectable(true, false)
	pcv.dashboard.Refresh()
	pcv.dashboard.Pages.SwitchToPage(pcv.dashboard.ROOT_WINDOW)
	pcv.dashboard.app.SetFocus(pcv.dashboard.PkgList)
}

// SetPackage to the current view to display its content.
func (pcv *PkgContentViewer) SetPackage(pkg *reposcan.PackageMeta) {
	pcv.pkgmeta = pkg
	for _, diffPackage := range pcv.dashboard.diff.ChangedPackages {
		if diffPackage.Package.Name() == pcv.pkgmeta.Package.Name() {
			pcv.diffmeta = diffPackage
			break
		}
	}

	pcv.updateWindowTitle()
	pcv.fillPackageContents()
}

func (pcv *PkgContentViewer) updateWindowTitle() {
	pcv.window.SetTitle(fmt.Sprintf(" Contents of package \"%s\" ", pcv.pkgmeta.Package.Name()))
}

/*
func (pcv *PkgContentViewer) isDiffViewable(name string) bool {
	return strings.ToLower(strings.Split(pcv.fileMime.MimeTypeByFilename(name), "/")[0]) == "text"
}
*/

// Lookup what kind of file it is (updated, removed, new or unchanged)
func (pcv *PkgContentViewer) getPkgFileDiffStatus(nfo *rpm.FileInfo) uint8 {
	if pcv.pkgmeta.Status == reposcan.STATUS_UPDATED && pcv.diffmeta != nil { // package
		for _, pi := range pcv.diffmeta.ChangedFiles {
			if pi.Name() == nfo.Name() {
				return reposcan.STATUS_UPDATED
			}
		}
		for _, pi := range pcv.diffmeta.NewFiles {
			if pi.Name() == nfo.Name() {
				return reposcan.STATUS_NEW
			}
		}
		for _, pi := range pcv.diffmeta.DeletedFiles {
			if pi.Name() == nfo.Name() {
				return reposcan.STATUS_REMOVED
			}
		}
	}
	return reposcan.STATUS_UNCHANGED
}

// Refresh the contents of the file list
func (pcv *PkgContentViewer) Refresh() {
	pcv.fillPackageContents()
}

func (pcv *PkgContentViewer) fillPackageContents() {
	pcv.fileIndex = make(map[int]*PkgFileInfo)
	pcv.fileList.Clear()
	pcv.fileList.SetSeparator('\u2502')
	maxwidth := 0
	if pcv.pkgmeta == nil {
		return
	}

	lastOffset := 0
	for offset, pkgFile := range pcv.pkgmeta.Package.Files() {
		pkgFileInfo := new(PkgFileInfo)
		pkgFileInfo.PkgFileInfo = pkgFile
		pkgFileInfo.Package = pcv.pkgmeta.Package

		lastOffset = offset
		if len(pkgFile.Name()) > maxwidth {
			maxwidth = len(pkgFile.Name())
		}

		var icon string
		if pcv.pkgmeta.Status == reposcan.STATUS_UPDATED {
			icon = "   "
		} else {
			icon = ""
		}

		style := new(tcell.Style).Foreground(pcv.fgColor)
		switch pcv.getPkgFileDiffStatus(&pkgFile) {
		case reposcan.STATUS_NEW:
			style = style.Bold(true).Foreground(tcell.ColorDarkCyan)
			icon = "\u22c4  "
			pkgFileInfo.NeedResolution = true
		case reposcan.STATUS_REMOVED:
			style = style.Bold(true)
			icon = "\u02df  "
		case reposcan.STATUS_UPDATED:
			style = style.Bold(true).Foreground(tcell.ColorDarkOrange)
			icon = "\u224f  "
			pkgFileInfo.NeedResolution = true
		default:
			style = style.Bold(false)
		}

		currentFileAcceptance := pcv.dashboard.resolver.CheckPackage(pcv.pkgmeta.Package.Name()).CheckFile(pkgFile.Name()).GetStatus()
		if pkgFileInfo.NeedResolution {
			switch currentFileAcceptance {
			case pkgresolve.STATUS_ACCEPTED:
				style = style.Bold(true).Foreground(pcv.arAcceptedColor)
				icon = "\u2713  "
			case pkgresolve.STATUS_REJECTED:
				style = style.Bold(true).Foreground(pcv.arRejectedColor)
				icon = "\u2715  "
			case pkgresolve.STATUS_IN_REVIEW:
				style = style.Bold(true).Foreground(pcv.arInReviewColor)
				icon = "\u25ce  "

			}
		}

		pcv.fileList.SetCell(offset, 0, crtview.NewTableCell(icon+pkgFile.Name()).
			SetStyle(style).SetSelectable(true).SetExpansion(1))
		pcv.fileList.SetCell(offset, 1, crtview.NewTableCell(pkgFile.Mode().String()).
			SetStyle(style.Bold(false).Foreground(tcell.ColorDefault)).SetSelectable(true))
		pcv.fileList.SetCell(offset, 2, crtview.NewTableCell(humanize.BigBytes((&big.Int{}).
			SetInt64(pkgFile.Size()))).SetStyle(style.Bold(false).Foreground(tcell.ColorDefault)).SetSelectable(true))

		pcv.fileIndex[offset] = pkgFileInfo
	}

	// Add table tail (doesn't redraw on terminal resize)
	ph := pcv.dashboard.term.Height
	if ph > 24 {
		ph -= ph / 4
	}
	ph = ph - lastOffset - 5
	if ph > 0 {
		for offset := 0; offset < ph; offset++ {
			empty := crtview.NewTableCell("")
			empty.SetSelectable(false)
			for i := 0; i < 3; i++ {
				pcv.fileList.SetCell(offset+lastOffset+1, i, empty)
			}
		}
	}

	// Draw header
	for offset, label := range []string{"Filename", "Perm", "Size"} {
		listItem := crtview.NewTableCell("").SetAlign(crtview.AlignLeft).
			SetTextColor(tcell.ColorYellow).SetSelectable(false).SetAttributes(tcell.AttrBold)

		switch offset {
		case 0:
			pad := 0
			if maxwidth > len(label) {
				pad = maxwidth - len(label)
			}
			label = label + strings.Repeat(" ", pad)
			listItem.SetExpansion(1)
		case 1:
			label = label + strings.Repeat(" ", 10-len(label))
		case 2:
			label = label + strings.Repeat(" ", 6-len(label))
		}
		listItem.SetText(label)
		pcv.fileListHeader.SetCell(0, offset, listItem)
	}
	// Reset selector
	pcv.fileList.Select(0, 0)
}

func (pcv *PkgContentViewer) initMainViewer() *crtview.Flex {
	pcv.viewHelp = crtview.NewTextView().SetBackgroundColor(pcv.bgColor).SetDynamicColors(true).SetTextAlign(crtview.AlignCenter).
		SetText("[yellow]Ctrl+V[silver]iew file (if available), [yellow]ENTER[silver]to resolve the change, [yellow]ESC[silver] to close.")
	return crtview.NewFlex().SetBackgroundColor(pcv.bgColor).SetDirection(crtview.FlexRow).
		AddItem(crtview.NewFlex().SetDirection(crtview.FlexRow).
			AddItem(pcv.fileListHeader, 1, 1, false).
			AddItem(pcv.fileList, 0, 1, true), 0, 1, true).
		AddItem(pcv.viewHelp, 1, 1, false)
}

func (pcv *PkgContentViewer) initUI() {
	pcv.window = crtview.NewWindow(pcv.initMainViewer()).SetBackgroundColor(pcv.bgColor)
	pcv.window.SetBorderColor(pcv.fgColor).SetTitleColor(pcv.fgColor).SetTitle(" Package Contents ")

	pcv.choiceWindow = NewPkgDiffResolve(pcv.dashboard)
	pcv.choiceWindow.GetWindow().SetPositionCenter().Hide()

	pcv.blameWindow = NewGitBlameView(pcv.dashboard)
	pcv.srcLogWindow = NewGitLogView(pcv.dashboard)
	pcv.commitViewWindow = NewGitCommitView(pcv.dashboard)
	pcv.alertWindow = NewAlertWindow(pcv.dashboard, pcv.fileList).SetAlertSize(40, 6)

	pcv.wm = crtview.NewWindowManager().SetFullScreen(true).
		Add(pcv.window).
		Add(pcv.choiceWindow.GetWindow()).
		Add(pcv.blameWindow.GetWindow()).
		Add(pcv.alertWindow.GetWindow()).
		Add(pcv.srcLogWindow.GetWindow()).
		Add(pcv.commitViewWindow.GetWindow())
}

func (pcv *PkgContentViewer) Focus(func(p crtview.Primitive)) {
	pcv.dashboard.app.SetFocus(pcv.fileList)
}

func (pcv *PkgContentViewer) Draw(screen tcell.Screen) {
	var tc string
	if screen.Colors() == 0x100 {
		tc = "silver"
		pcv.window.SetBackgroundColor(tcell.ColorMediumBlue).SetBorderColor(tcell.ColorLightSkyBlue)
		pcv.fileList.SetBackgroundColor(tcell.ColorMediumBlue).SetBordersColor(tcell.ColorLightSkyBlue)
		pcv.viewHelp.SetBackgroundColor(tcell.ColorMediumBlue)
		pcv.fileListHeader.SetBackgroundColor(tcell.ColorMediumBlue)
	} else {
		tc = "silver"
	}
	helpLabel := ""
	if pcv.pkgmeta.Status == reposcan.STATUS_UPDATED {
		helpLabel = fmt.Sprintf("[yellow]ENTER[%s] to resolve the change, ", tc)
	} else if pcv.pkgmeta.Status == reposcan.STATUS_NEW || pcv.pkgmeta.Status == reposcan.STATUS_REMOVED {
		helpLabel = fmt.Sprintf("[yellow]ENTER[%s] to resolve the package, ", tc)
	}
	pcv.viewHelp.SetText(fmt.Sprintf("%s[yellow]ESC[%s] to close", helpLabel, tc))

	w, h := screen.Size()
	x, y := 0, 0
	wx, wy := x, y
	ww, wh := w, h

	// Respect VT100 (1978) users :)
	if w > 100 {
		wsp := w / 3
		ww -= wsp
		wx = wsp / 2
	}
	if h > 24 {
		hsp := h / 4
		wh -= hsp
		wy = hsp / 2
	}

	// Add status
	statusLabel := "[%s][[%s] %s package[%s], %s ]"
	switch pcv.pkgmeta.Status {
	case reposcan.STATUS_NEW:
		statusLabel = fmt.Sprintf(statusLabel, tc, "darkred", "New", tc, "complete review required")
	case reposcan.STATUS_REMOVED:
		statusLabel = fmt.Sprintf(statusLabel, tc, "darkgrey", "Removed", tc, "no resoltion needed")
	case reposcan.STATUS_UNCHANGED:
		statusLabel = fmt.Sprintf(statusLabel, tc, "white", "Unchanged", tc, "nothing to do here")
	case reposcan.STATUS_UPDATED:
		statusLabel = fmt.Sprintf(statusLabel, tc, "orange", "Changed", tc, "diff review and resolutions required")
	}
	pcv.window.SetStatus(statusLabel).SetSize(ww, wh).SetPositionCenter().SetPosition(wx, wy)
	pcv.wm.Draw(screen)
}
