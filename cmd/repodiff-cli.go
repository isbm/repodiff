package main

import (
	"fmt"
	"os"
	"path"
	"sort"

	wzlib_logger "github.com/infra-whizz/wzlib/logger"
	"github.com/isbm/go-nanoconf"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/isbm/repodiff/gitbind"
	"gitlab.com/isbm/repodiff/pkgflow"
	"gitlab.com/isbm/repodiff/pkgresolve"
	"gitlab.com/isbm/repodiff/reposcan"
)

var VERSION string = "0.2.1"

// Setup logger level
func setLoggerLevel(ctx *cli.Context) {
	var level logrus.Level
	if ctx.Bool("debug") {
		level = logrus.DebugLevel
	} else {
		if ctx.Bool("pkgdiff") || ctx.Bool("quiet") {
			level = logrus.FatalLevel
		} else {
			level = logrus.InfoLevel
		}
	}
	wzlib_logger.GetCurrentLogger().SetLevel(level)
	wzlib_logger.GetCurrentLogger().Debug("Using debug mode")
}

// Run repo diff
func doRepoDiff(ctx *cli.Context) error {
	setLoggerLevel(ctx)
	conf := nanoconf.NewConfig(ctx.String("config"))
	if ctx.String("basepath") != "" && ctx.String("updatepath") != "" {
		rldf := reposcan.NewRepoLayoutDiff(ctx.String("type")).SetBaseRepo(ctx.String("basepath")).SetUpdateRepo(ctx.String("updatepath"))
		diffData, err := rldf.Diff()
		if err != nil {
			wzlib_logger.GetCurrentLogger().Errorf("Error getting diff: %s", err.Error())
			os.Exit(1)
		}

		if ctx.Bool("pkgdiff") {
			pkgs := []string{}
			for _, pkg := range diffData.NewPackages {
				pkgs = append(pkgs, path.Base(pkg.Package.Path()))
			}
			for _, ud := range diffData.ChangedPackages {
				pkgs = append(pkgs, path.Base(ud.Package.Path()))
			}
			sort.Strings(pkgs)
			for _, pkgname := range pkgs {
				fmt.Println(pkgname)
			}
			if len(pkgs) > 0 {
				os.Exit(1)
			}
			return nil
		}

		resolver, err := pkgresolve.NewPkgAcceptanceResolver().Setup(ctx.String("resolve"),
			gitbind.NewPkgDiffGitResolver().InitBranch())
		if err != nil {
			wzlib_logger.GetCurrentLogger().Errorf("Error initialising resolve document: %s", err.Error())
			os.Exit(1)
		}

		if ctx.Bool("workflow") {
			pkgflow.NewUIDashboard(resolver, conf).LoadRepodiff(diffData).Run()
		}
	} else {
		cli.ShowAppHelpAndExit(ctx, 1)
	}

	return nil
}

// Main
func main() {
	appname := "repodiff"
	confpath := nanoconf.NewNanoconfFinder(appname).DefaultSetup(nil)
	app := &cli.App{
		Version: VERSION,
		Name:    appname,
		Usage:   "Repository diff. Finds out differences between two same repos.",
		Action:  doRepoDiff,
	}

	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:     "config",
			Aliases:  []string{"c"},
			Usage:    "Path to the confinguration file",
			Required: false,
			Value:    confpath.SetDefaultConfig(confpath.FindFirst()).FindDefault(),
		},
		&cli.BoolFlag{
			Name:    "debug",
			Aliases: []string{"d"},
			Usage:   "Increase logging to a debug level",
		},
		&cli.BoolFlag{
			Name:    "quiet",
			Aliases: []string{"q"},
			Usage:   "Quiet logging (off)",
		},
		&cli.StringFlag{
			Name:    "type",
			Aliases: []string{"t"},
			Usage:   "Type of packaging. Choices: 'rpm', 'deb'",
			Value:   "rpm",
		},
		&cli.StringFlag{
			Name:    "basepath",
			Aliases: []string{"b"},
			Usage:   "Base path to the repository source packages (current)",
		},
		&cli.StringFlag{
			Name:    "updatepath",
			Aliases: []string{"u"},
			Usage:   "Update path to the repository source packages (next)",
		},
		&cli.StringFlag{
			Name:    "resolve",
			Aliases: []string{"r"},
			Usage:   "Path to the resolve workflow document",
			Value:   "release-acceptance.yaml",
		},
		&cli.BoolFlag{
			Name:    "workflow",
			Aliases: []string{"w"},
			Usage:   "UI workflow",
		},
		&cli.BoolFlag{
			Name:    "pkgdiff",
			Aliases: []string{"f"},
			Usage:   "Show list of changed packages",
		},
	}

	if err := app.Run(os.Args); err != nil {
		wzlib_logger.GetCurrentLogger().Errorf("General error: %s", err.Error())
	}
}
